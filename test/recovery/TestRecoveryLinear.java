package recovery;
/**
 * bradley Eddowes
 * 
 * for testing the linear recovery method
 */
import static org.junit.Assert.*;

import org.junit.Test;

public class TestRecoveryLinear 
{
	/*
	 *  Start Section for Strategy Pattern Tests
	 */
	@Test
	public void noRecoveryWhenNotHurt()
	{
		RecoveryLinear rl = new RecoveryLinear(3);
		int maxLifePts = 30;
		int result = rl.calculateRecovery(maxLifePts, maxLifePts);
		assertEquals(maxLifePts,result);

		//make sure no recovery when equal
		assertEquals(40, rl.calculateRecovery(40, 40));
		//make sure it only goes to the max points
		assertEquals(40, rl.calculateRecovery(38, 40));
		// make sure it properly adds 3 points of life
		assertEquals(33, rl.calculateRecovery(30, 40));
		//make sure it does not recover when dead
		assertEquals(0, rl.calculateRecovery(0, 40));
		
	}
}
