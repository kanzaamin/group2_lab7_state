package recovery;
/**
 * bradley eddowes 
 * 
 * for testing the none recovery 
 */
import static org.junit.Assert.*;

import org.junit.Test;

public class TestRecoveryNone 
{
	/*
	 *  Start Section for Strategy Pattern Tests
	 */
	@Test
	public void testRecoveryNone()
	{
		RecoveryBehavior heal = new RecoveryNone();
		assertEquals(100,heal.calculateRecovery(100,100));
		//test to make sure no healing occurs and current life points are returned unchanged
		assertEquals(70,heal.calculateRecovery(70, 100));
	}
}
