package recovery;
/**
 * bradley eddowes
 * 
 * test fractional recovery method 
 */
import static org.junit.Assert.*;

import org.junit.Test;

public class TestRecoveryFractional 
{
	/*
	 *  Start Section for Strategy Pattern Tests
	 */
	@Test
	public void recoveryFractional()
	{
		RecoveryFractional rf = new RecoveryFractional(10);
		assertEquals(100,rf.calculateRecovery(100, 100));
		assertEquals(100,rf.calculateRecovery(98,100));
		assertEquals(110,rf.calculateRecovery(100,200));
		assertEquals(0,rf.calculateRecovery(0, 200));
	}
}
