package weapon;

/**
 * @Bradley Eddowes & Isaiah English & Kanza Amin Test
 */

import static org.junit.Assert.*;

import org.junit.Test;

public class testWeaponAttachments 
{
	/*
	 * this block testes the effects of attachments on weapons, doing all combinations listed in Lab 4 as well checking names and attachment counts.
	 */
	@Test
	public void testAttachmentFunctions()
	{
		Weapon pistol = new Pistol();
		assertEquals(8, pistol.fire(10));
		assertEquals("Pistol",pistol.getName());
		pistol = new Pistol();
		pistol = new Scope(pistol);
		assertEquals(13, pistol.fire(10));
		assertEquals("Pistol + Scope",pistol.getName());
		pistol = new Scope(pistol);
		pistol.updateTime(0);
		pistol.reload();
		assertEquals(23, pistol.fire(10));
		assertEquals("Pistol + Scope + Scope",pistol.getName()); 
		assertEquals(2,pistol.getNumAttachments());
		pistol = new Pistol();
		pistol = new Scope(pistol);
		pistol = new Stabilizer(pistol);
		pistol.updateTime(0);
		pistol.reload();
		assertEquals(16, pistol.fire(10));
		pistol = new Pistol();
		pistol = new Scope(pistol);
		pistol = new PowerBooster(pistol);
		assertEquals(26, pistol.fire(10));
		pistol.updateTime(0);
		pistol.reload();
		
		Weapon plasma = new PlasmaCannon();
		assertEquals(50,plasma.fire(20));
		plasma.updateTime(0);
		plasma.reload();
		plasma = new Stabilizer(plasma);
		assertEquals(62,plasma.fire(20));
		plasma = new PlasmaCannon();
		plasma = new Stabilizer(plasma);
		plasma = new Scope(plasma);
		assertEquals(67,plasma.fire(30));
		plasma = new PlasmaCannon();
		plasma = new Stabilizer(plasma);
		plasma = new Stabilizer(plasma);
		assertEquals(77,plasma.fire(10));
		assertEquals("Plasma Cannon + Stabilizer + Stabilizer",plasma.getName());
		plasma = new PlasmaCannon();
		plasma = new Stabilizer(plasma);
		plasma = new PowerBooster(plasma);
		assertEquals(124,plasma.fire(10));
		
		Weapon chain = new ChainGun();
		assertEquals(7,chain.fire(15));
		chain = new PowerBooster(chain);
		chain.updateTime(0);
		//39 actual ammo now
		assertEquals(13,chain.fire(15));
		chain = new ChainGun();
		chain = new PowerBooster(chain);
		chain = new Scope(chain);
		assertEquals(35,chain.fire(35));
		chain = new ChainGun();
		chain = new PowerBooster(chain);
		chain = new Stabilizer(chain);
		assertEquals(25,chain.fire(20));
		chain = new ChainGun();
		chain = new PowerBooster(new PowerBooster(chain));
		assertEquals("Chain Gun + Power Booster + Power Booster",chain.getName());
		assertEquals(20,chain.fire(10));
	}
}
