package weapon;
/**
 * @Bradley Eddowes & Isaiah English & Kanza Amin Test
 * 
 * used to create test with a basic, non specific weapon
 */
public class MockWeapon extends GenericWeapon
{
	public MockWeapon()
	{
			super("Mock Weapon",10,25,2,10);
	}
	
	//Overridden fire class
	public int fire(int distance)
	{
		int damage = 0;
		if (getActualAmmo() > 0 && getShotsLeft() > 0)
		{
			if (distance <= getMaxRange())
			{
				setActualAmmo(-1);
				setShotsLeft(-1);
				damage = (int)((double)(this.getBaseDamage()) * (double)(this.getMaxRange() - distance + 5)/this.getMaxRange());
			}
			else
			{
				setActualAmmo(-1);
				setShotsLeft(-1);
			}
		}
		return damage;
	}
}
