package weapon;

/**
 * @Bradley Eddowes & Isaiah English & Kanza Amin Test
 */

import static org.junit.Assert.*;

import org.junit.Test;

public class testGenericWeapon 
{
	/*
	 * this test the generic funcions of a non specific weapon before attachments
	 */
	@Test
	public void testGenericWeaponFunctions()
	{
		GenericWeapon mock = new MockWeapon();
		//check for basic damage
		assertEquals(10, mock.fire(5));
		//check for zerop damage outside of range
		assertEquals(0,mock.fire(26));
		//at this point we have no shots left for this round so damage should be zero - ammo 8
		assertEquals(0,mock.fire(5));
		//update round to allow for more shots
		mock.updateTime(0);
		assertEquals(12,mock.fire(0));
		assertEquals(0,mock.fire(100));
		//not shots left this round - ammo 6
		assertEquals(6,mock.getActualAmmo());
		assertEquals(0,mock.getShotsLeft());
		mock.updateTime(0);
		mock.fire(0);
		mock.fire(0);
		mock.updateTime(0);
		mock.fire(0);
		mock.fire(0);
		mock.updateTime(0);
		mock.fire(0);
		mock.fire(0);
		//should be out of ammo
		assertEquals(0, mock.getActualAmmo());
		mock.reload();
		//ammo reloaded to max
		assertEquals(mock.getMaxAmmo(),mock.getActualAmmo());
		//rate of fire limited shots still
		assertEquals(0,mock.getShotsLeft());
		mock.updateTime(0);
		//new round, replenished shots
		assertEquals(2,mock.getShotsLeft());
	}
}
