package lifeform;
/**
 * Brad Eddowes
 * 
 * Test the alien class
 */
import recovery.RecoveryFractional;
import recovery.RecoveryLinear;
import gameplay.SimpleTimer;
import static org.junit.Assert.*;

import org.junit.Test;

import exceptions.negativeRecoveryRate;

public class TestAlien 
{
	/*
	 * Starts section for lab6
	 */
	
	@Test //tests that aliens max speed is 2
	public void testDefaultMove() 
	{
		Alien bob = new Alien("bob", 100);	
		assertEquals(2, bob.getMoveSpeed());
	}
	
	/*
	 *  Start Section for Singleton Pattern
	 */
	@Test // addition test with timer to simulate combat even better
	public void testCombatWithTimer() throws InterruptedException
	{
		int healthTrack = 10;
		int prevHealth = 0;
		SimpleTimer st = new SimpleTimer(1000);
		Alien bob = new Alien("bob",100,new RecoveryLinear(20),30,1);
		Human steve = new Human("steve",100,20,2);
		assertEquals(2,steve.getAttack(0));
		st.addTimeObserver(bob);
		st.addTimeObserver(steve);
		bob.takeHit(90);
		st.start();
		Thread.sleep(250); // Se we are the 1/4th a second different
		//for(int x=0;x<5;x++)
		while ( bob.getLifePoints() != 100)
		{
			//System.out.println("Previous Health: " + prevHealth + " Current Health: " + bob.getLifePoints() + " Health Tracker: " + healthTrack);
			if (prevHealth != bob.getLifePoints())
			{
			assertEquals(healthTrack,bob.getLifePoints()); //watches bobs health
			bob.takeHit(steve.getAttack(0));
			steve.takeHit(bob.getAttack(0));
			healthTrack += 18; //add the amount bob should heal to check next loop
			prevHealth = bob.getLifePoints();
			}
			Thread.sleep(1000);// wait for the next time change before moving to next loop
		}
		// check that bob made it to 100 health because he recovered more health than 
		// he took damage per round
		assertEquals(100, bob.getLifePoints());
		//check that steve took some damage during the fight.
		assertEquals(50, steve.getLifePoints());
	}
	// test constructor and default attack strength
	@Test
	public void testAttackFunctionality()
	{
		Alien bob = new Alien("Bob",100);
		// default strength should be 10
		assertEquals(10, bob.getAttack(0));
		bob.takeHit(20);
		assertEquals(80,bob.getLifePoints());
	}
	// test recovery behavior in combat simulating change in time
	@Test
	public void testCombatRecoveryBehavior() throws negativeRecoveryRate
	{
		Alien gray = new Alien("gray",100,new RecoveryLinear(5),0,0);
		gray.takeHit(50);
		SimpleTimer roundCounter1 = new SimpleTimer(1000);
		roundCounter1.addTimeObserver(gray);
		roundCounter1.timeChanged();
		//make sure alien did not heal on first time change 
		assertEquals(50,gray.getLifePoints());
		roundCounter1.timeChanged();
		// make sure alien did not heal on second time change
		assertEquals(50,gray.getLifePoints());
		
		gray.setRecoveryRate(2);
		roundCounter1.timeChanged();
		//make sure alien did not heal on first time change 
		assertEquals(50,gray.getLifePoints());
		roundCounter1.timeChanged();
		// make sure alien healed on second time change
		assertEquals(55,gray.getLifePoints());
		
		gray.setRecoveryRate(7);
		roundCounter1.timeChanged();
		roundCounter1.timeChanged();
		roundCounter1.timeChanged();
		roundCounter1.timeChanged();
		// make sure alien hasn't healed yet
		assertEquals(55,gray.getLifePoints());
		roundCounter1.timeChanged();
		roundCounter1.timeChanged();
		roundCounter1.timeChanged();
		// see if it healed on the seventh
		assertEquals(60,gray.getLifePoints());
		
		roundCounter1.removeTimeObserver(gray);
		
		gray.setRecoveryRate(1);
		roundCounter1.timeChanged();
		roundCounter1.timeChanged();
		roundCounter1.timeChanged();
		roundCounter1.timeChanged();
		//make sure he didn't heal
		assertEquals(60,gray.getLifePoints());
	}
	// test for negativeRecovery exception
	@Test(expected = negativeRecoveryRate.class)
	public void testNegativeRecoveryRateException() throws negativeRecoveryRate
	{
		Alien gray = new Alien("gray",100,new RecoveryLinear(5),0,0);
		gray.setRecoveryRate(-23);	
	}
	/*
	 *  Start Section for Strategy Pattern Tests
	 */
	@Test
	public void testInitialization()
	{
		Alien greenMan = new Alien("LGM", 200);
		assertEquals("LGM", greenMan.getName());
		assertEquals(200, greenMan.getLifePoints());
	}
	
	@Test
	public void testAlienRecover()
	{
		Alien greenMan = new Alien("LGM",200, new RecoveryLinear(3));
		greenMan.setCurrentLifePoints(100);
		assertEquals(100,greenMan.getLifePoints());
		greenMan.recover();
		assertEquals(103,greenMan.getLifePoints());
		
		//**EXTRA TESTS**
		Alien blueMan = new Alien(200);
		//test basic constructor
		assertEquals("Default Name", blueMan.getName());
		blueMan.setCurrentLifePoints(100);
		blueMan.recover();
		assertEquals(100, blueMan.getLifePoints());
		// test recovery fractional
		Alien redMan = new Alien("DOOOOD", 200, new RecoveryFractional(20));
		redMan.setCurrentLifePoints(100);
		redMan.recover();
		assertEquals(120, redMan.getLifePoints());
	}
}
