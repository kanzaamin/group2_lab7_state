package lifeform;

public class MockLifeForm extends LifeForm
{
	public MockLifeForm(String name, int points)
	{
		this(name, points, 999);
		this.setMoveSpeed(3);
	}
	
	public MockLifeForm(String name, int points, int strength)
	{
		super(name, points, strength);
		this.setMoveSpeed(3);
	}

}
