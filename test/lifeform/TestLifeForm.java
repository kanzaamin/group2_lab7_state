package lifeform;
 
import static org.junit.Assert.*;
 
import org.junit.Test;

import environment.Environment;
import exceptions.EnvironmentException;
import exceptions.ExcessiveAttachmentException; 
import gameplay.SimpleTimer;
import weapon.Pistol;
import weapon.PlasmaCannon;
import weapon.PowerBooster;
import weapon.Scope;
/**
 *     Tests the functionality provided by the LifeForm class
 * @Bradley Eddowes & Isaiah English & Kanza Amin Test
 *  
 *
 */
 
public class TestLifeForm  
{
 
	/*
	 * Starts tests for lab6
	 */
	
	//tests that the default direction is north
	@Test
	public void testDirection()
	{
		LifeForm entity = new MockLifeForm("entity", 10, 10);
		assertEquals(10, entity.getLifePoints());
		assertEquals(Facing.North, entity.getDirection());
	}
	
	//tests that lifeForm can change direction from North to South
	@Test
	public void testChangeDirection()
	{
		LifeForm entity = new MockLifeForm("entity", 10, 10);
		assertEquals(Facing.North, entity.getDirection());
		entity.changeDirection(Facing.South);
		assertEquals(Facing.South, entity.getDirection());
	}
	
	/*
	 * Start test block for Singleton Pattern Lab 5
	 * 
	 * Modifications made to original tests in this labe to use environemnt get distance method instead of range class
	 */
    /*
     * Start test block for Decorator Pattern Lab 4
     *  
     * This set of tests checks that a life form can use the weapons, they function normally and that the damage used by life form is based on specific
     * conditions
     */
 
    /*
     * Will test is lifeForm can pick up a weapon
     */
    @Test 
    public void testPickUpWeapon() throws ExcessiveAttachmentException, EnvironmentException
    {
    		Environment world = Environment.getInstance(4, 4);
            LifeForm dude = new MockLifeForm("Dude", 100, 99);
            LifeForm dudet = new MockLifeForm("Dudet", 100, 99);
    		world.addLifeForm(0, 0, dude);
    		world.addLifeForm(0, 2, dudet);
            dude.setWeapon(new PowerBooster(new Scope(new Pistol())));
            assertEquals(26,dude.getAttack(world.getDistance(dude, dudet)));
            Environment.clearBoard();
    }
    /*
     * Will test if LifeForm has a weapon, it cannot pick up another weapon: PISTOL
     */
    @Test
    public void testCantPickUpWeapon() throws ExcessiveAttachmentException, EnvironmentException
    {
    	Environment world = Environment.getInstance(4, 4);
        LifeForm dude = new MockLifeForm("Dude", 100, 99);
        LifeForm dudet = new MockLifeForm("Dudet", 100, 99);
		world.addLifeForm(0, 0, dude);
		world.addLifeForm(0, 2, dudet);
            dude.setWeapon(new PowerBooster(new Scope(new Pistol())));
            assertEquals(26,dude.getAttack(world.getDistance(dude, dudet)));
            dude.setWeapon(new Pistol());
            dude.reload(); // reload to make sure that the missing shot does not effect damage
            //attack should change because dude doesn't pick up a new weapon when he has one.
            assertEquals(26,dude.getAttack(world.getDistance(dude, dudet)));
           Environment.clearBoard();
    }
    /*
     * Will test that LifeForm can drop a weapon
     */
    @Test
    public void testDropWeapon() throws ExcessiveAttachmentException, EnvironmentException {
    	Environment world = Environment.getInstance(4, 4);
        LifeForm dude = new MockLifeForm("Dude", 100, 99);
        LifeForm dudet = new MockLifeForm("Dudet", 100, 99);
		world.addLifeForm(0, 0, dude);
		world.addLifeForm(0, 1, dudet);
            dude.setWeapon(new PowerBooster(new Scope(new Pistol())));
            assertEquals(36,dude.getAttack(world.getDistance(dudet, dude)));
            dude.removeWeapon();
            assertEquals(99,dude.getAttack(world.getDistance(dudet, dude)));
            //make sure dude does damage based on its strength if it had successfully dropped its weapon
         Environment.clearBoard();
    }
    /*
     * Will test LifeForm to use weapon for damage, if weapon has ammo
     */
    @Test
    public void testAmmo() throws ExcessiveAttachmentException, EnvironmentException  
    {
    	Environment world = Environment.getInstance(4, 4);
        LifeForm dude = new MockLifeForm("Dude", 100, 99);
        LifeForm dudet = new MockLifeForm("Dudet", 100, 99);
		world.addLifeForm(0, 0, dude);
		world.addLifeForm(0, 2, dudet);
            dude.setWeapon(new PlasmaCannon());
            assertEquals(50, dude.getAttack(world.getDistance(dude, dudet)));
            dude.updateTime(0);
            assertEquals(37, dude.getAttack(world.getDistance(dude, dudet)));
            dude.updateTime(0);
            assertEquals(25, dude.getAttack(world.getDistance(dude, dudet)));
            dude.updateTime(0);
            assertEquals(12, dude.getAttack(world.getDistance(dude, dudet)));
            dude.updateTime(0);
        Environment.clearBoard();
 
    }
    /*
     * Will test LifeForm to use attack strength for damage if weapon has no ammo
     */
    @Test
    public void testNoAmmo() throws ExcessiveAttachmentException, EnvironmentException {
    	Environment world = Environment.getInstance(4, 4);
        LifeForm dude = new MockLifeForm("Dude", 100, 99);
        LifeForm dudet = new MockLifeForm("Dudet", 100, 99);
		world.addLifeForm(0, 0, dude);
		world.addLifeForm(0, 1, dudet);
            dude.setWeapon(new PlasmaCannon());
            assertEquals(50, dude.getAttack(world.getDistance(dude, dudet)));
            //update time because rate of fire is one
            dude.updateTime(0);
            dude.getAttack(world.getDistance(dude, dudet));
            //update time because rate of fire is one
            dude.updateTime(0);
            dude.getAttack(world.getDistance(dude, dudet));
            //update time because rate of fire is one
            dude.updateTime(0);
            dude.getAttack(world.getDistance(dude, dudet));
            //update time because rate of fire is one
            dude.updateTime(0);
            assertEquals(99,dude.getAttack(world.getDistance(dude, dudet)));
 
            //weapon should now be out of ammo, dude should use strength, if in range
            Environment.clearBoard();
    }
 
    /*
     * Will test if range > 5 feet, weapon does no damage
     */
    @Test
    public void testRange() throws ExcessiveAttachmentException, EnvironmentException{
    	Environment world = Environment.getInstance(4, 4);
        LifeForm dude = new MockLifeForm("Dude", 100, 99);
        LifeForm dudet = new MockLifeForm("Dudet", 100, 99);
		world.addLifeForm(0, 0, dude);
		world.addLifeForm(0, 1, dudet);
            dude.setWeapon(new PlasmaCannon());
            dude.updateTime(0);
            assertEquals(50, dude.getAttack(world.getDistance(dude, dudet)));
            //update time because rate of fire is one
            dude.updateTime(0);
            dude.getAttack(world.getDistance(dude, dudet));
            //update time because rate of fire is one
            dude.updateTime(0);
            dude.getAttack(world.getDistance(dude, dudet));
            //update time because rate of fire is one
            dude.updateTime(0);
            dude.getAttack(world.getDistance(dude, dudet));
            //update time because rate of fire is one
            dude.updateTime(0);
            assertEquals(99,dude.getAttack(world.getDistance(dude, dudet)));
            world.removeLifeForm(0, 1);
            world.addLifeForm(1, 1, dudet);
            assertEquals(0,dude.getAttack(world.getDistance(dude, dudet)));
            Environment.clearBoard();
    }
 
 
    /*
     * start section for observer pattern tests Lab 3
     */
    @Test
    public void testAttackFuncgtionality()
    {
            MockLifeForm bob = new MockLifeForm("BOB", 100, 10);
            MockLifeForm steve = new MockLifeForm("STEVE", 100, 20);
            assertEquals(10, bob.getAttack(0));
            steve.takeHit(bob.getAttack(0));
            assertEquals(90,steve.getLifePoints());
            steve.takeHit(90);
            bob.takeHit(steve.getAttack(0));
            // make sure bob didn't take any damage from dead steve
            assertEquals(100, bob.getLifePoints());
 
            SimpleTimer time = new SimpleTimer();
            time.addTimeObserver(bob);
            assertEquals(0,bob.getTime());
            time.timeChanged();
            assertEquals(1,bob.getTime());
    }
 
    /*
     *  Start Section for Strategy Pattern Tests Lab 2
     */
    /**
     * When a LifeForm is created, it should know its name and how
     * many life points it has.
     */
    @Test
    public void testInitialization() {
            MockLifeForm entity;
            entity = new MockLifeForm("Bob", 40);
            assertEquals("Bob", entity.getName());
            assertEquals(40, entity.getLifePoints());
            entity.takeHit(30);
            //successfully removes 30 lifepoints
            assertEquals(10, entity.getLifePoints());
            entity.takeHit(0);
            //0 damage does not effect lifepoints
            assertEquals(10, entity.getLifePoints());
            entity.takeHit(-11);
            //negative damage is not counted **EXTRA TEST**
            assertEquals(10, entity.getLifePoints());
            entity.takeHit(20);
            //does not go below 0 lifepoints
            assertEquals(0, entity.getLifePoints());
            entity.takeHit(47);
            //can take hit at 0 lifepoints without going to 0
            assertEquals(0, entity.getLifePoints());
    }
 
}