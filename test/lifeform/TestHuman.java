package lifeform;
/**
 * Bradley Eddowes
 * 
 * test the human class
 */
import lifeform.Human;

import static org.junit.Assert.*;

import org.junit.Test;

public class TestHuman 
{
	/*
	 * Starts tests for lab6
	 */
	
	@Test //tests that maxSpeed is 3
	public void testMaxSpeed()
	{
		Human rick = new Human ("Rick", 12, 10);
		assertEquals(3, rick.getMoveSpeed());	
	}
	
	/*
	 * Start section of lab5
	 */
		@Test
		public void testAttackFunctionality()
		{
			Human Joe = new Human("Joe",100,20);
			//make sure the default strength is 5
			assertEquals(5, Joe.getStrength());
			//make sure armor absorbs the damage
			Joe.takeHit(15);
			assertEquals(100, Joe.getLifePoints());
			//make sure the armor only absorbs some of the damage if damage is greater
			Joe.takeHit(25);
			assertEquals(95, Joe.getLifePoints());
			// make sure it absorbs all when damage = armor
			Joe.takeHit(20);
			assertEquals(95, Joe.getLifePoints());
		}
	/*
	 *  Start Section for Strategy Pattern Tests
	 */
		@Test
		public void testInitialization()
		{
			Human man = new Human("steve", 100, 40);
			assertEquals(40,man.getArmorPoints());
			assertEquals(100,man.getLifePoints());
			man.setArmorPoints(-12);
			// make sure armor min is 0
			assertEquals(0,man.getArmorPoints());
		}

}
