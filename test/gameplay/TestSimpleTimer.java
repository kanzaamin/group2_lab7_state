package gameplay;

import static org.junit.Assert.*;

import org.junit.Test;
/*
 * brad Eddowes
 * class for testing TimeObserver and using Timer
 */
class MockSimpleTimerObserver implements TimeObserver
{
	public int myTime = 0;
	
	public void updateTime(int time)
	{
		myTime = time;
	}
	
	/*
	 * method for checking that the observer recieved the time correctly
	 */
	public int getTime()
	{
		return myTime;
	}
}

/*
 * Test creating and using at timer
 */
public class TestSimpleTimer 
{
	/*
	 * Test basic time functions like adding and removing observers as well as updating their time
	 */
	@Test
	public void testSimpleTimerFunciton()
	{
		Timer time = new SimpleTimer();
		TimeObserver bob = new MockSimpleTimerObserver();
		time.addTimeObserver(bob);
		//bob was added and alerted of the current time
		assertEquals(0,bob.getTime());
		time.timeChanged();
		//timeChanged updated the time by one and told bob
		assertEquals(1,bob.getTime());
		time.removeTimeObserver(bob);
		time.timeChanged();
		//makes sure bob was removed by seeing if it was still called by timeChanged after being removed
		assertEquals(1,bob.getTime());
	}
	
	/**
	 * This Tests that SimpleTImer will update time once everySecond.
	 */
	@Test
	public void testSimpleTimerAsThread() throws InterruptedException
	{
		SimpleTimer st = new SimpleTimer(1000);
		st.start();
		Thread.sleep(250); // Se we are the 1/4th a second different
		for (int x=0;x<5;x++)
		{
			assertEquals(x,st.getRound()); //assumes round starts at 0
			//System.out.println("Got round: " + st.getRound());
			Thread.sleep(1000);// wait for the next time change
		}
	}
}
