
import org.junit.runner.RunWith; 
import org.junit.runners.Suite;

/**
 * runs all of the tests in this project 
 * @Bradley Eddowes Test
 *	
 */
@RunWith(Suite.class)
@Suite.SuiteClasses(
		{
			lifeform.TestLifeForm.class,
			environment.TestCell.class,
			environment.TestEnvironment.class,
			lifeform.TestHuman.class,
			recovery.TestRecoveryNone.class,
			recovery.TestRecoveryLinear.class,
			recovery.TestRecoveryFractional.class,
			lifeform.TestAlien.class,
			gameplay.TestSimpleTimer.class,
			weapon.testGenericWeapon.class,
			weapon.testWeaponAttachments.class,
			displayGUIs.TestCommandGui.class,
			displayGUIs.testCommands.class
		})

public class AllGameTests {
}
