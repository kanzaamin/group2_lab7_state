package AIContent;

import static org.junit.Assert.*;

import org.junit.Test;

import environment.Environment;
import lifeform.Human;
import lifeform.LifeForm;
import weapon.Pistol;
import weapon.Weapon;

public class TestDeadState {

	
	/**
	 * Tests when a lifeform was holding a weapon
	 */
	@Test
	public void testWithWeapon() {
		Environment e = Environment.getInstance(10, 10);
		LifeForm Isaiah = new Human("Isaiah", 5, 5);
		AIContext AI = new AIContext(Isaiah);
		Weapon pistol = new Pistol();
		
		e.addLifeForm(0, 0, Isaiah);
		Isaiah.setWeapon(pistol);
		
		AI.setDeadState();
		assertEquals("Dead State", AI.getState()); 
		AI.takeTurn(1);
		
		assertEquals("No Weapon State", AI.getState()); //tests to make sure it's in deadstate
		assertNotNull(Isaiah.getWeapon()); //tests to make sure there is no weapon
	}
	
	
	/**
	 * Tests when lifeform had no weapon
	 */
	@Test
	public void testWithOutWeapon() {
		Environment e = Environment.getInstance(10, 10);
		LifeForm Isaiah = new Human("Isaiah", 5, 5);
		AIContext AI = new AIContext(Isaiah);
		
		e.addLifeForm(0, 0, Isaiah);
		
		AI.setDeadState();
		assertEquals("Dead State", AI.getState()); //tests that AI is in deadstate
		AI.takeTurn(1);
		
		assertEquals("No Weapon State", AI.getState());	//tests that AI is in NoWeaponState
		assertNull(Isaiah.getWeapon()); 	//tests to make sure there are no weapons
	}
	
	
	/**
	 * Tests to see that the lifeform changes to a new cell when 
	 * it respawns
	 */
	@Test
	public void testChangeCell()
	{
		Environment e = Environment.getInstance(5, 5);
		
		LifeForm bob = new Human("Bob", 15, 15);
		AIContext AI = new AIContext(bob);
		e.addLifeForm(0, 0, bob);
		
		
		
		AI.setDeadState();
		assertEquals("Dead State", AI.getState()); //makes sure lifeform is in deadstate
		AI.takeTurn(1);
		assertEquals("No Weapon State", AI.getState()); //makes sure lifeform has no weapon
		
		assertNotEquals(AI, e.getLifeForm(0, 0)); //makes sure that the lifeform has moved
		assertNotNull(e.getLifeForm(0, 0));
		
	}

}
