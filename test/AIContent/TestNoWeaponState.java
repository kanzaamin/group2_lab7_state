package AIContent;

import static org.junit.Assert.*;

import org.junit.Test;

import environment.Environment;
import lifeform.Facing;
import lifeform.Human;
import lifeform.LifeForm;
import weapon.Pistol;

/**
 * 
 * @author Group 2
 * 
 * Tests for all of the no weapon state
 */
public class TestNoWeaponState {

	/**
	 * Tests that the lifeform should pick up the weapon if its 
	 * in the same cell
	 */
	@Test
	public void testWeaponInCell() {
		Environment.clearBoard();
		Environment world = Environment.getInstance(10,10);
		LifeForm bob = new Human("Bob", 100, 100);
		Pistol p1 = new Pistol();
		AIContext AI = new AIContext(bob);
		world.addLifeForm(0, 0, bob); // place bob
		world.addWeaponToCell(p1, 1, 0, 0);
		assertEquals("No Weapon State", AI.getState());
		AI.takeTurn(1);
		assertEquals("Has Weapon State", AI.getState());
	}
	
	/**
	 * Tests that the lifeform will move when in the no weapon state
	 * and if the cell its in does not have a weapon
	 */
	@Test
	public void testWeaponNotInCell() {
		Environment.clearBoard();
		Environment world = Environment.getInstance(10,10);
		LifeForm bob = new Human("Bob", 100, 100);
		bob.changeDirection(Facing.East);
		Pistol p1 = new Pistol();
		AIContext AI = new AIContext(bob);
		world.addLifeForm(0, 0, bob); // place bob
		world.addWeaponToCell(p1, 1, 5, 0);
		assertEquals("No Weapon State", AI.getState());
		AI.takeTurn(1);
		assertEquals(null, world.getLifeForm(0, 0)); // The lifeform is no longer there
	}

}
