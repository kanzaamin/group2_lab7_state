package AIContent;

import static org.junit.Assert.*;

import org.junit.Test;

import displayGUIs.EnvironmentGui;
import environment.Environment;
import gameplay.SimpleTimer;
import gameplay.Timer;
import lifeform.Alien;
import lifeform.Human;
import lifeform.LifeForm;
import recovery.RecoveryFractional;
import recovery.RecoveryLinear;

/**
 * 
 * @author Group 2
 * Tests for the simulator
 *
 */
public class TestSimulator {


	/**
	 * Tests that with 10 lifeforms we get 5 aliens and 5 humans
	 */
	@Test
	public void testGettingCorrectLifeforms()
	{
		int humans = 0;
		int aliens = 0;
		Environment.clearBoard();
		Environment world = Environment.getInstance(10,10);
		Simulator s1 = new Simulator(10, world);
		
		for(int i = 0; i < world.getColumns(); i++)
		{
			for(int t = 0; t < world.getRows(); t++)
			{
				if(world.getLifeForm(i, t) instanceof Human)
				{
					humans++;
				}
				else if(world.getLifeForm(i, t) instanceof Alien)
				{
					aliens++;
				}
			}
		}
		assertEquals(5, humans);
		assertEquals(5, aliens);
	}
	
	/**
	 * Tests that all the lifeforms are different
	 */
	@Test
	public void testCheckingLifeformsDifferent()
	{
		Environment.clearBoard();
		Environment world = Environment.getInstance(10,10);
		Simulator s1 = new Simulator(10, world);
		for(int n = 0; n < 10; n++)
		{
			for(int i = 0; i < 10; i++)
			{
				if(n == i) // if n = i then its the same lifeform
				{
					assertEquals(s1.getLifeForm(n), s1.getLifeForm(i));
				}
				else
				{
					assertNotEquals(s1.getLifeForm(n), s1.getLifeForm(i));
				}
			}
		}
	}
	
	/**
	 * Tests that the random numbers for the ranges of recoverys and humans armor are in
	 * the range
	 */
	@Test
	public void testRanges()
	{
		Human temp;
		Alien temp1;
		Environment.clearBoard();
		Environment world = Environment.getInstance(10,10);
		Simulator s1 = new Simulator(10, world);
		for(int i = 0; i < 10; i++)
		{
			if(s1.getLifeForm(i) instanceof Human) // if n = i then its the same lifeform
			{
				temp = (Human) s1.getLifeForm(i);
				assertTrue(temp.getArmorPoints() >=0 && temp.getArmorPoints() <= 9);
			}
			else
			{
				temp1 = (Alien) s1.getLifeForm(i);
				if(temp1.getRecoveryRate() instanceof RecoveryLinear)
				{
					assertTrue(temp1.getRecoveryNumber() >=0 && temp1.getRecoveryNumber() <= 20);
				}
				else if(temp1.getRecoveryRate() instanceof RecoveryFractional)
				{
					assertTrue(temp1.getRecoveryNumber() <=5 && temp1.getRecoveryNumber() >= 1);
					
				}
			}
		}
		
	}
	

	
	/**
	 * Tests that all the weapons are different
	 */
	@Test
	public void testDifferentWeapons()
	{
		Environment.clearBoard();
		Environment world = Environment.getInstance(10,10);
		Simulator s1 = new Simulator(10, world);
		for(int n = 0; n < 10; n++)
		{
			for(int i = 0; i < 10; i++)
			{
				if(n == i) // if n = i then its the same lifeform
				{
					assertEquals(s1.getWeapon(n), s1.getWeapon(i));
				}
				else
				{
					assertNotEquals(s1.getWeapon(n), s1.getWeapon(i));
				}
			}
		}
	}
	
	/**
	 * Tests that all lifeforms have a weapon. This uses the same code from the previous 
	 * test so the weapons are confirmed random
	 */
	@Test
	public void testLifeformsHaveWeapons()
	{
		Environment.clearBoard();
		Environment world = Environment.getInstance(10,10);
		Simulator s1 = new Simulator(10, world);
		for(int n = 0; n < 10; n++)
		{
			assertNotEquals(null, s1.getLifeForm(n).getWeapon());
		}
	}
}
