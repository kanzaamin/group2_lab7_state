package AIContent;

import static org.junit.Assert.*;

import org.junit.Test;

import environment.Environment;
import lifeform.Human;
import lifeform.LifeForm;
import weapon.Pistol;
import weapon.Weapon;

public class TestOutOfAmmoState {

	/**
	 * Tests if weapon fully reloads when OutOfAmmo state is called
	 */
	@Test
	public void testReload() {
		Environment e = Environment.getInstance(5, 5);
		LifeForm bob = new Human("Bob", 15, 15);
		Weapon p = new Pistol();
		AIContext AI = new AIContext(bob);
		e.addLifeForm(0,0, bob);
		bob.setWeapon(p);
		p.setActualAmmo(0);
		
		AI.setOutOfAmmo();
		assertEquals("Out Of Ammo State", AI.getState());
		AI.takeTurn(1);
		
		assertEquals(10, p.getActualAmmo());
	}

	
	/**
	 * Test if state changes to HasWeapon
	 */
	@Test
	public void testChangeState()
	{
		Environment e = Environment.getInstance(5, 5);
		LifeForm bob = new Human("Bob", 15, 15);
		Weapon p = new Pistol();
		AIContext AI = new AIContext(bob);
		e.addLifeForm(0,0, bob);
		bob.setWeapon(p);
		p.setActualAmmo(0);
		
		AI.setOutOfAmmo();
		assertEquals("Out Of Ammo State", AI.getState()); //tests that state is out oif ammo
		AI.takeTurn(1);
		
		assertEquals("Has Weapon State", AI.getState());	//tests that state is hasWeapon
	}
	
	
	/**
	 * Test if state changes to dead if lifeform is dead
	 */
	@Test
	public void testDead()
	{
		Environment e = Environment.getInstance(5, 5);
		LifeForm bob = new Human("Bob", 0, 0);
		Weapon p = new Pistol();
		AIContext AI = new AIContext(bob);
		e.addLifeForm(0,0, bob);
		bob.setWeapon(p);
		p.setActualAmmo(0);
		
		AI.setOutOfAmmo();
		assertEquals("Out Of Ammo State", AI.getState()); //tests that state is out oif ammo
		AI.takeTurn(1);
		
		assertEquals("Dead State", AI.getState());	//tests that state is DeadState
	}
	
}
