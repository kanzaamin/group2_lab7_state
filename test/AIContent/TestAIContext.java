package AIContent;

import static org.junit.Assert.*;

import org.junit.Test;

import environment.Environment;
import lifeform.Human;
import lifeform.LifeForm;
import weapon.Pistol;


/**
 * 
 * @author Group 2
 * This class holds all of the tests for AIContext. These just test that we can transition between 
 * states
 *
 */
public class TestAIContext {

	/**
	 * Tests the transition from no weapon state to weapon state
	 */
	@Test
	public void testAIContextOne()
	{
		Environment.clearBoard();
		Environment world = Environment.getInstance(10,10);
		LifeForm bob = new Human("Bob", 100, 100);
		Pistol p1 = new Pistol();
		AIContext AI = new AIContext(bob);
		
		world.addLifeForm(0, 0, bob); // place bob
		world.addWeaponToCell(p1, 1, 0, 0);
		assertEquals("No Weapon State", AI.getState());
		AI.takeTurn(1);
		assertEquals("Has Weapon State", AI.getState());
	}
	
	/**
	 * Tests the transition of Has weapon state to out of ammo state
	 * by taking the ammo away from the weapon
	 * 
	 */
	@Test
	public void testAIContextTwo()
	{
		Environment.clearBoard();
		Environment world = Environment.getInstance(10,10);
		LifeForm bob = new Human("Bob", 100, 100);
		Pistol p1 = new Pistol();
		AIContext AI = new AIContext(bob);
		
		world.addLifeForm(0, 0, bob); // place bob
		world.addWeaponToCell(p1, 1, 0, 0);
		assertEquals("No Weapon State", AI.getState());
		AI.takeTurn(1);
		assertEquals("Has Weapon State", AI.getState());
		p1.setActualAmmo(-10);
		AI.takeTurn(2);
		assertEquals("Out Of Ammo State", AI.getState());
	}
	
	/**
	 * Tests that the lifeform will go into the dead state after losing all lifepoints
	 * and then gets respawned without a weapon
	 */
	@Test
	public void testAIContextThree()
	{
		Environment.clearBoard();
		Environment world = Environment.getInstance(10,10);
		LifeForm bob = new Human("Bob", 100, 100);
		Pistol p1 = new Pistol();
		AIContext AI = new AIContext(bob);
		
		world.addLifeForm(0, 0, bob); // place bob
		world.addWeaponToCell(p1, 1, 0, 0);
		assertEquals("No Weapon State", AI.getState());
		bob.takeHit(10000);
		AI.takeTurn(1);
		assertEquals("Dead State", AI.getState());
		AI.takeTurn(2);
		assertEquals("No Weapon State", AI.getState());
	}

	
}
