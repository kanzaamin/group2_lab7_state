package environment;

import lifeform.Alien;
import lifeform.Facing;
import lifeform.Human;
import lifeform.LifeForm;
import lifeform.MockLifeForm;
import weapon.ChainGun;
import weapon.Pistol;
import weapon.Weapon;

import static org.junit.Assert.*;

import org.junit.Test;

import exceptions.EnvironmentException;

/**
 * 	
 * @Bradley Eddowes & Isaiah English & Kanza Amin Test Environment
 *
 */


public class TestEnvironment {

	/*
	 * Start section for lab6
	 */

	
	//tests all the interactions that can happen in North movement
	@Test
	public void testNorthMovement()
	{
		Environment worl = Environment.getInstance(10, 10);
		LifeForm racoon = new MockLifeForm("racoon", 10);
		LifeForm obstacle = new MockLifeForm("rock", 10);
		assertEquals(3, racoon.getMoveSpeed());
		worl.addLifeForm(9, 9, racoon);
		worl.addLifeForm(6, 9, obstacle);
		
		//moved less than maxSpeed because of obstacle 
		worl.moveLifeForm(racoon);
		assertEquals(racoon, worl.getLifeForm(7, 9));
		
		//moved maxSpeed over an obstacle
		worl.moveLifeForm(racoon);
		assertEquals(racoon, worl.getLifeForm(4, 9));
		
		//moved without an obstacle 
		worl.moveLifeForm(racoon);
		assertEquals(racoon, worl.getLifeForm(1, 9));
		
		
		//moves less then maxSpeed because it hits the edge
		worl.moveLifeForm(racoon);
		assertEquals(racoon, worl.getLifeForm(0, 9));
		
		//dosen't move anywhere, because it's at the edge already
		worl.moveLifeForm(racoon);
		assertEquals(racoon, worl.getLifeForm(0, 9));
		
	} 
	
	//tests all the interactions that can happen in South movement
	@Test
	public void testSouthMovement()
	{
		Environment worl = Environment.getInstance(10, 10);
		LifeForm racoon = new MockLifeForm("racoon", 10);
		LifeForm obstacle = new MockLifeForm("rock", 10);
		assertEquals(3, racoon.getMoveSpeed());
		worl.addLifeForm(0, 9, racoon);
		worl.addLifeForm(3, 9, obstacle);
		racoon.changeDirection(Facing.South);
		
		//moved less than maxSpeed because of obstacle 
		worl.moveLifeForm(racoon);
		assertEquals(racoon, worl.getLifeForm(2, 9));
		
		//moved maxSpeed over an obstacle
		worl.moveLifeForm(racoon);
		assertEquals(racoon, worl.getLifeForm(5, 9));
		
		//moved without an obstacle 
		worl.moveLifeForm(racoon);
		assertEquals(racoon, worl.getLifeForm(8, 9));
		
		
		//moves less then maxSpeed because it hits the edge
		worl.moveLifeForm(racoon);
		assertEquals(racoon, worl.getLifeForm(9, 9));
		
		//dosen't move anywhere, because it's at the edge already
		worl.moveLifeForm(racoon);
		assertEquals(racoon, worl.getLifeForm(9, 9));
		
	}
	
	//tests all the interactions that can happen in East movement
	@Test
	public void testEastMovement()
	{
		Environment worl = Environment.getInstance(10, 10);
		LifeForm racoon = new MockLifeForm("racoon", 10);
		LifeForm obstacle = new MockLifeForm("rock", 10);
		assertEquals(3, racoon.getMoveSpeed());
		worl.addLifeForm(9, 0, racoon);
		worl.addLifeForm(9, 3, obstacle);
		racoon.changeDirection(Facing.East);
		
		//moved less than maxSpeed because of obstacle 
		worl.moveLifeForm(racoon);
		assertEquals(racoon, worl.getLifeForm(9, 2));
		
		//moved maxSpeed over an obstacle
		worl.moveLifeForm(racoon);
		assertEquals(racoon, worl.getLifeForm(9, 5));
		
		//moved without an obstacle 
		worl.moveLifeForm(racoon);
		assertEquals(racoon, worl.getLifeForm(9, 8));
		
		
		//moves less then maxSpeed because it hits the edge
		worl.moveLifeForm(racoon);
		assertEquals(racoon, worl.getLifeForm(9, 9));
		
		//dosen't move anywhere, because it's at the edge already
		worl.moveLifeForm(racoon);
		assertEquals(racoon, worl.getLifeForm(9, 9));
		
	}
	
	//tests all the interactions that can happen in West movement
	@Test
	public void testWestMovement()
	{
		Environment worl = Environment.getInstance(10, 10);
		LifeForm racoon = new MockLifeForm("racoon", 10);
		LifeForm obstacle = new MockLifeForm("rock", 10);
		assertEquals(3, racoon.getMoveSpeed());
		worl.addLifeForm(9, 9, racoon);
		worl.addLifeForm(9, 6, obstacle);
		racoon.changeDirection(Facing.West);
		
		//moved less than maxSpeed because of obstacle 
		worl.moveLifeForm(racoon);
		assertEquals(racoon, worl.getLifeForm(9, 7));
		
		//moved maxSpeed over an obstacle
		worl.moveLifeForm(racoon);
		assertEquals(racoon, worl.getLifeForm(9, 4));
		
		//moved without an obstacle 
		worl.moveLifeForm(racoon);
		assertEquals(racoon, worl.getLifeForm(9, 1));
		
		
		//moves less then maxSpeed because it hits the edge
		worl.moveLifeForm(racoon);
		assertEquals(racoon, worl.getLifeForm(9, 0));
		
		//dosen't move anywhere, because it's at the edge already
		worl.moveLifeForm(racoon);
		assertEquals(racoon, worl.getLifeForm(9, 0));
		
	}
	
	
	/*
	 * start section for singleton pattern test lab 5
	 */
	@Test
	public void initializationSinglton()
	{
		Environment world = Environment.getInstance(5, 5);
		assertEquals(25, world.getSize());
		world = Environment.getInstance(10, 10);
		//should still be the same
		assertEquals(25, world.getSize());
		Environment.clearBoard();
	}
	
	@Test
	public void addRemoveWeapon()
	{
		Environment world = Environment.getInstance(5, 5);
		Weapon weapon1 = new Pistol();
		Weapon weapon2 = new ChainGun();
		world.addWeaponToCell(weapon1, 1, 0, 0);
		world.addWeaponToCell(weapon2, 1, 1, 1);
		//weapons should be placed
		assertEquals("Pistol", (world.removeWeaponFromCell(1, 0, 0)).getName());
		assertEquals("Chain Gun", (world.removeWeaponFromCell(1, 1, 1)).getName());
		//weapon should no longer be there
		assertEquals(null, world.removeWeaponFromCell(1, 0, 0));
		Environment.clearBoard();
	}
	@Test
	public void rangeSameRow() throws EnvironmentException
	{
		Environment world = Environment.getInstance(5, 5);
		LifeForm dude = new MockLifeForm("Dude", 100, 99);
        LifeForm dudet = new MockLifeForm("Dudet", 100, 99);
        world.addLifeForm(0, 0, dudet);
        world.addLifeForm(0, 4, dude);
        assertEquals(20,world.getDistance(dude, dudet));
        Environment.clearBoard();
	}
	@Test
	public void rangeSameColumn() throws EnvironmentException
	{
		Environment world = Environment.getInstance(5, 5);
		LifeForm dude = new MockLifeForm("Dude", 100, 99);
        LifeForm dudet = new MockLifeForm("Dudet", 100, 99);
        world.addLifeForm(0, 0, dudet);
        world.addLifeForm(3, 0, dude);
        assertEquals(15,world.getDistance(dude, dudet));
        Environment.clearBoard();
	}
	@Test
	public void rangeDiagonal() throws EnvironmentException
	{
		Environment world = Environment.getInstance(5, 5);
		LifeForm dude = new MockLifeForm("Dude", 100, 99);
        LifeForm dudet = new MockLifeForm("Dudet", 100, 99);
        world.addLifeForm(0, 0, dudet);
        world.addLifeForm(3, 3, dude);
        assertEquals(21,world.getDistance(dude, dudet));
        Environment.clearBoard();
	}
	/*
	 *  Start Section for Strategy Pattern Tests
	 */
	@Test
	public void TestInitialization() 
	{
		Environment area = Environment.getInstance(1,1);
		assertNull(area.getLifeForm(0,0));
		Environment.clearBoard();
		//Environment area2 = new Environment(2,3);
		Environment area2 = Environment.getInstance(2,3);
		assertNull(area2.getLifeForm(1, 2));
		Environment.clearBoard();


	}
	/**
	 * Test adding and removing a LifeForm 
	 */
	@Test
	public void testAddRemoveLifeForm()
	{
		Environment.clearBoard();
		Environment area = Environment.getInstance(2, 3);
		assertNull(area.getLifeForm(1, 1));
		MockLifeForm bob = new MockLifeForm("bob",45);
		assertTrue(area.addLifeForm(1,0,bob));
		//System.out.println(area.addLifeForm(1,0,bob));
		LifeForm bobCopy = area.getLifeForm(1,0);
		assertEquals("bob",bobCopy.getName());
		assertTrue(area.removeLifeForm(1,0));
		assertNull(area.getLifeForm(1, 0));
		Environment.clearBoard();
	}
	/**
	 * method to test boundaries checks.
	 */
	@Test
	public void testBounds()
	{
		Environment.clearBoard();
		//Environment area = new Environment(3,3);
		Environment area = Environment.getInstance(3, 3);
		assertNull(area.getLifeForm(3, 3)); // out of bounds
		MockLifeForm bob = new MockLifeForm("Bob",10);
		MockLifeForm joe = new MockLifeForm("Joe",12);
		assertTrue(area.addLifeForm(2, 2, bob)); // true because in bounds
		assertFalse(area.addLifeForm(2, 3, joe)); // false because out of bounds
		
		assertTrue(area.removeLifeForm(2, 2)); // true becuase in bounds
		assertFalse(area.removeLifeForm(3, 3)); // false because out of bounds
		
	}
		@Test
	public void testHuman()
	{
		Environment.clearBoard();
		//Environment area = new Environment(3,3);
		Environment area = Environment.getInstance(3, 3);
		assertNull(area.getLifeForm(3, 3)); // out of bounds
		Human bob = new Human("Bob",10, 8);
		assertTrue(area.addLifeForm(2, 2, bob)); 
	}
	@Test
	public void testAlien()
	{
		Environment.clearBoard();
		//Environment area = new Environment(3,3);
		Environment area = Environment.getInstance(3, 3);
		assertNull(area.getLifeForm(3, 3)); // out of bounds
		Alien joe = new Alien("Joe",12);
		assertTrue(area.addLifeForm(2, 2, joe)); 
	}	



}
