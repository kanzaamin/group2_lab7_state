package environment;
//import lifeform.LifeForm;
import lifeform.MockLifeForm;
import weapon.ChainGun;
import weapon.Pistol;
import weapon.PlasmaCannon;

import static org.junit.Assert.*;

import org.junit.Test;
/**
 * The test cases for the Cell class
 * @Bradley Eddowes & Isaiah English & Kanza Amin Test	
 */



public class TestCell {
/**
 * At initialization, the Cell should be empty and not contain a LifeForm.
 */
	/*
	 * start section for singleton pattern test lab 5
	 */
	@Test
	public void testInitialization()
	{
		Cell spot = new Cell();
		assertNull(spot.getLifeForm());
		assertNull(spot.removeWeaponFromCell(1));
		assertNull(spot.removeWeaponFromCell(2));
	}
	@Test
	public void addRemoveWeapons()
	{
		Cell spot = new Cell();
		spot.addWeaponToCell(new Pistol(), 1);
		spot.addWeaponToCell(new ChainGun(), 2);
		assertEquals("Pistol", (spot.removeWeaponFromCell(1)).getName());
		assertEquals("Chain Gun", (spot.removeWeaponFromCell(2)).getName());
		spot.addWeaponToCell(new Pistol(), 1);
		spot.addWeaponToCell(new ChainGun(), 2);
		assertFalse(spot.addWeaponToCell(new PlasmaCannon(), 2));
	}

	/*
	 *  Start Section for Strategy Pattern Tests
	 */
	@Test
	public void TestInitialization() {
		Cell cell = new Cell();
		assertNull(cell.getLifeForm());
	}
/**
 * checks to see if we change the LifeForm held by the Cell that 
 * getLifeForm properly responds to this change
 */
	@Test
	public void testSetLifeForm()
	{
		MockLifeForm bob = new MockLifeForm("Bob", 40);
		MockLifeForm fred = new MockLifeForm("Fred", 40);
		Cell cell = new Cell();
		// the cell is empty so this should work.
		boolean success = cell.addLifeForm(bob);
		assertTrue(success);
		assertEquals(bob,cell.getLifeForm());
		// The cell is not empty so this should fail.
		success = cell.addLifeForm(fred);
		assertFalse(success);
		assertEquals(bob,cell.getLifeForm());
		
		
	}
}
