package displayGUIs;

import static org.junit.Assert.*;

import org.junit.Test;

import environment.Environment;
import lifeform.Facing;
import lifeform.Human;
import weapon.Pistol;


/**
 * @author Andrew McCoy
 * This holds all of the test for the buttons
 * on the commandGui. This tests that all of them
 * do the correct job for a simple situation.
 * 
 */
public class TestCommandGui {

	/**
	 * This test is to make sure the move button actually works
	 */
	@Test
	public void testLifeFormMoveButton()
	{
		Environment.clearBoard();
		Environment world = Environment.getInstance(10,10);
		Human bob = new Human("Bob",100,30);
		CommandGui steve = new CommandGui(world);
		world.addLifeForm(9, 0, bob); // place bob
		world.setHighlightedCell(0, 9); //highlights bobs cell
		steve.update();
		assertEquals(world.getLifeForm(9, 0),bob); // check that bob was placed
		steve.move.doClick();
		assertEquals(world.getLifeForm(6, 0), bob);
	}
	
	/**
	 * Tests that the pick up button works when the weapon is in bobs cell
	 */
	@Test
	public void testLifeFormPickUpWeaponButton()
	{
		Environment.clearBoard();
		Environment world = Environment.getInstance(10,10);
		Human bob = new Human("Bob",100,30);
		Pistol gun1 = new Pistol();
		CommandGui steve = new CommandGui(world);
		
		world.addWeaponToCell(gun1, 1, 0, 9); //Places gun in bobs cell
		world.addLifeForm(9, 0, bob); // place bob
		assertEquals(world.getLifeForm(9, 0),bob); // check that bob was placed
		world.setHighlightedCell(0, 9); //highlights bobs cell
		
		assertEquals(bob.getWeapon(), null);
		
		steve.update();
		steve.pick_up_one.doClick();
		
		assertEquals(bob.getWeapon(), gun1);
	}
	
	/**
	 * This tests that bob can pick up a weapon and drop the weapon
	 */
	@Test
	public void testLifeFormDropWeapon()
	{
		Environment.clearBoard();
		Environment world = Environment.getInstance(10,10);
		Human bob = new Human("Bob",100,30);
		Pistol gun1 = new Pistol();
		CommandGui steve = new CommandGui(world);
		
		world.addWeaponToCell(gun1, 1, 0, 9); //Places gun in bobs cell
		world.addLifeForm(9, 0, bob); // place bob
		assertEquals(world.getLifeForm(9, 0),bob); // check that bob was placed
		world.setHighlightedCell(0, 9); //highlights bobs cell
		
		assertEquals(bob.getWeapon(), null);
		
		steve.update();
		steve.pick_up_one.doClick();
		
		assertEquals(bob.getWeapon(), gun1);
		//Bob will drop the weapon
		steve.update();
		steve.drop.doClick();
		
		assertEquals(bob.getWeapon(), null);
	}
	
	/**
	 * Tests that the button used for reloading actually works
	 */
	@Test
	public void testLifeFormReloadButton()
	{
		Environment.clearBoard();
		Environment world = Environment.getInstance(10,10);
		Human bob = new Human("Bob",100,30);
		Pistol gun1 = new Pistol();
		CommandGui steve = new CommandGui(world);
		assertEquals(10, gun1.getActualAmmo());
		bob.setWeapon(gun1);
		bob.getAttack(3);
		assertEquals(9, gun1.getActualAmmo());
		world.addLifeForm(9, 0, bob); // place bob
		world.setHighlightedCell(0, 9); //highlights bobs cell
		steve.update();
		steve.reload.doClick();
		assertEquals(10, gun1.getActualAmmo());
	}
	
	/**
	 * Tests that we can change the direction of the lifeform
	 * in the world
	 */
	@Test
	public void testDirectionButtons()
	{
		Environment.clearBoard();
		Environment world = Environment.getInstance(10,10);
		Human bob = new Human("Bob",100,30);
		CommandGui steve = new CommandGui(world);
		
		world.addLifeForm(9, 0, bob); // place bob
		world.setHighlightedCell(0, 9); //highlights bobs cell
		steve.update();
		
		steve.right.doClick();
		assertEquals(Facing.East, bob.getDirection());
		steve.down.doClick();
		assertEquals(Facing.South, bob.getDirection());
		steve.left.doClick();
		assertEquals(Facing.West, bob.getDirection());
		steve.top.doClick();
		assertEquals(Facing.North, bob.getDirection());
	}
	
	/**
	 * Tests that the lifeform can attack another lifeform
	 * that is in the cell in front of them
	 */
	@Test
	public void testAttack()
	{
		Environment.clearBoard();
		Environment world = Environment.getInstance(10,10);
		Human bob = new Human("Bob",100,30);
		Human Dave = new Human("Dave",90,0);
		CommandGui steve = new CommandGui(world);
		
		world.addLifeForm(1, 0, bob); // place bob
		world.addLifeForm(0, 0, Dave); // place bob
		assertEquals(90, Dave.getLifePoints());
		world.setHighlightedCell(0, 1); //highlights bobs cell
		steve.update();
		
		steve.attack.doClick();	//Attacks Dave
		
		assertEquals(85, Dave.getLifePoints());
		
	}

}
