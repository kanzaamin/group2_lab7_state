package displayGUIs;

import static org.junit.Assert.*;

import org.junit.Test;

import environment.Environment;
import lifeform.Alien;
import lifeform.Facing;
import lifeform.Human;
import weapon.ChainGun;
import weapon.Pistol;
import weapon.PlasmaCannon;
import weapon.Weapon;

/*
 * group 2
 * @Bradley Eddowes & Isaiah English & Kanza Amin Test
 */

public class testCommands 
{
	@Test // test that the move command moves the selected lifeform
	public void testMove()
	{
		Environment.clearBoard();
		Environment world = Environment.getInstance(10,10);
		Human bob = new Human("Bob",100,30);
		world.addLifeForm(9, 0, bob); // place bob
		assertEquals(world.getLifeForm(9, 0),bob); // check that bob was placed
		Command move = new Move(bob);
		move.execute(); // move bob
		assertEquals(world.getLifeForm(6,0), bob); //check that bob moved
	}
	@Test //reloads weapon
	public void testReload()
	{
		Human bob = new Human("Bob",100,30);
		Weapon plasmaCannon = new PlasmaCannon();
		bob.setWeapon(plasmaCannon);
		assertEquals(bob.getAttack(10),50); // only one shot per clip
		assertEquals(bob.getAttack(10),0); // no shots left
		Command reload = new Reload(bob);
		bob.updateTime(0); // allow bob to shoot more with round update
		reload.execute(); // reload bobs gun
		assertEquals(bob.getAttack(10),50); // gun fires again
	}
	@Test // test turning the lifeform in all 4 directions
	public void testTurning()
	{
		Human bob = new Human("Bob",100,30);
		assertEquals(bob.getDirection(),Facing.North); // default direction
		Command turnEast = new Turn(Facing.East,bob); // create turn commands
		Command turnWest = new Turn(Facing.West,bob);
		Command turnSouth = new Turn(Facing.South,bob);
		Command turnNorth = new Turn(Facing.North,bob);
		turnEast.execute();
		assertEquals(bob.getDirection(),Facing.East);
		turnWest.execute();
		assertEquals(bob.getDirection(),Facing.West);
		turnSouth.execute();
		assertEquals(bob.getDirection(),Facing.South);
		turnNorth.execute();
		assertEquals(bob.getDirection(),Facing.North);
	}
	@Test //test the attack command
	public void testAttack()
	{
		Human bob = new Human("Bob",100,30);
		Alien steve = new Alien("Steve",100);
		Weapon plasmaCannon = new PlasmaCannon();
		bob.setWeapon(plasmaCannon);//give bob a gun
		Environment.clearBoard();
		Environment world = Environment.getInstance(10, 10);
		world.addLifeForm(9, 0, bob); // put bob in bottom left corner, bob faces north by default
		world.addLifeForm(7, 0, steve); // put steve two cells (10 feet) above bob
		Command attack = new Attack(bob);
		assertEquals(steve.getLifePoints(),100);
		attack.execute();
		assertEquals(steve.getLifePoints(),50);
	}
	@Test /// test the drop command
	public void testDrop()
	{
		Human bob = new Human("Bob",100,30);
		Weapon plasmaCannon = new PlasmaCannon();
		bob.setWeapon(plasmaCannon);//give bob a gun
		assertEquals(bob.getWeapon(), plasmaCannon); // make sure bob got the gun
		Environment.clearBoard();
		Environment world = Environment.getInstance(10, 10);
		world.addLifeForm(9, 0, bob); // put bob in bottom left corner, with no weapon in slot
		assertEquals(world.getWeapon(0, 0, 9),null); // confirm there is no gun in slot 1
		assertEquals(world.getWeapon(2, 0, 9),null); // confirm there is no gun in slot 2
		Command drop = new Drop(bob, world);
		drop.execute(); //execute drop command
		assertEquals(bob.getWeapon(),null); //make sure bob no longer has a gun
		assertEquals(world.getWeapon(1, 0, 9),plasmaCannon); //make sure weapon was dropped
		Weapon pistol = new Pistol();
		bob.setWeapon(pistol);
		assertEquals(bob.getWeapon(),pistol);
		drop.execute();
		assertEquals(bob.getWeapon(),null); //make sure bob no longer has a gun
		assertEquals(world.getWeapon(2, 0, 9),pistol); //make sure weapon was dropped
		//now there are weapons in both slots, lets try and drop another gun from bob
		Weapon chainGun = new ChainGun(); // make the gun 
		bob.setWeapon(chainGun); // give him the gun
		assertEquals(bob.getWeapon(),chainGun); // make sure he got the gun
		drop.execute(); // try and drop for a third time
		assertEquals(bob.getWeapon(),chainGun);//make sure bob still has weapon because drop could not execute
		assertEquals(world.getWeapon(1, 0, 9),plasmaCannon); // make sure first dropped weapon is still there
		assertEquals(world.getWeapon(2, 0, 9),pistol); // make sure second dropped weaopn is still there 
	}
	@Test // test the acquire command
	public void testAcquire()
	{
		Human bob = new Human("Bob",100,30);
		Weapon plasmaCannon = new PlasmaCannon();
		assertEquals(bob.getWeapon(), null); // make sure bob does not have a gun
		Environment.clearBoard();
		Environment world = Environment.getInstance(10, 10);
		world.addLifeForm(0, 0, bob); // add bob to cell where there is no life form
		Command acquireOne = new Acquire(1,bob,world);
		Command acquireTwo = new Acquire(2,bob,world);
		acquireOne.execute();
		assertEquals(bob.getWeapon(), null); // make sure bob does not have a gun
		acquireTwo.execute();
		assertEquals(bob.getWeapon(), null); // make sure bob does not have a gun
		world.addWeaponToCell(plasmaCannon, 1, 0, 0);
		acquireOne.execute();
		assertEquals(bob.getWeapon(), plasmaCannon); // make sure bob picked up the gun
		assertEquals(world.getWeapon(1, 0, 0), null); // make sure the slot is now empty
		Weapon pistol = new Pistol();
		world.addWeaponToCell(pistol, 1, 0, 0);//add another weapon to slot one
		acquireOne.execute(); // pick up from slot one again
		assertEquals(bob.getWeapon(), pistol); // make sure bob picked up the gun in the slot and dropped gun he had
		assertEquals(world.getWeapon(1, 0, 0), plasmaCannon); // make sure the slot now has the gun bob had and not the pistol
	}
}
