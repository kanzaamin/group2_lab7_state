/**
 * A Cell that can hold a LifeForm.
 * @bradley Eddowes & Isaiah English & Kanza Amin Test
 *THIS IS JUST A TEST FOR A COMMIT. Lets see if it works! 10-23-17
 */
package environment;
import lifeform.LifeForm;
import weapon.Weapon;

public class Cell {
	private LifeForm currentEntity;
	protected Weapon weapon1;
	protected Weapon weapon2;
	/**
	 * @return the LifeForm in this Cell
	 */
	public LifeForm getLifeForm()
	{
		return currentEntity;
	}
/**
 * Tries to add the LifeForm to the Cell. will not add if a 
 * LifeForm is already present.
 * @return true if the LifeForm was added to the Cell, false otherwise.
 */
	public boolean addLifeForm(LifeForm entity)
	{
		if (currentEntity == null)
				{
					currentEntity = entity;
					entity.setPlaced(true);
					return true;
				}
		else 
			return false;
	}
	
	public void removeLifeForm()
	{
		currentEntity.setPlaced(false);
		currentEntity = null;
	}
/***************************
 * Lab 5 Starts here!
 * @param weapon
 ***************************/
	/**
	 * method used to add a weapon to a cell where the cell will check to see which spot is available and add the weapon to it.
	 * @param weapon the weapon being added to the cell
	 * @index the weapon slot to have a weapon added to it
	 */
	public boolean addWeaponToCell(Weapon weapon, int index)
	{
		boolean x = false;
		
		if (index == 1)
		{
			if(weapon1==null)
			{
				weapon1=weapon;
				x = true;
			}
		}
		else if (index == 2)
		{
			if(weapon2==null)
			{
				weapon2=weapon;
				x=true;
			}
		}
		return x;
	}
	/**
	 * method used by life forms to exchange a weapon with on of the weapons in the cell
	 * @param weapon the weapon being dropped by the lifeform
	 * @param index the weapon the lifeform wants to pick up
	 * @return the weapon that the lifeform is picking up
	 */
	public Weapon exchangeWeapon(Weapon weapon, int index)
	{
		Weapon temporary=null;
		if(index == 1)
		{
			temporary = weapon1;
			weapon1 = weapon;
		}else if(index == 2)
		{
			temporary = weapon2;
			weapon2=weapon;
		}
		return temporary;
		
	}
	/**
	 * method to remove and return the weapon from a cell
	 * @param index which weapon to remove 
	 * @return the weapon being removed 
	 */
	public Weapon removeWeaponFromCell(int index)
	{
		Weapon weapon = null;
		if(index == 1)
		{
			weapon=weapon1;
			weapon1=null;
		}
		else if (index == 2)
		{
			weapon = weapon2;
			weapon2 = null;
		}
		return weapon;
	}
	
	public Weapon getWeapon(int index)
	{
		Weapon weapon = null;
		if(index == 1)
		{
			weapon=weapon1;
		}
		else if (index == 2)
		{
			weapon = weapon2;
		}
		return weapon;
	}
}
	
