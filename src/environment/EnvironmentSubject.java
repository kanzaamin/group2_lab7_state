package environment;

import gameplay.TimeObserver;

public interface EnvironmentSubject 
{
	//method to add observers to list 
	public void addEnvironmentObserver(EnvironmentObserver observer);
	//method to remove specific observers 
	public void removeEnvironmentObserver(EnvironmentObserver observer);
	//method to update observers in array of changes
	public void environmentChanged();
}
//this is the final test