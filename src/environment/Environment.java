/**
 * Creates an Environment to hold cell(s)
 * @Bradley Eddowes & Isaiah English & Kanza Amin Test
 *
 */
package environment;
import lifeform.Facing;
import lifeform.Human;
import lifeform.LifeForm;

import java.util.ArrayList;

import exceptions.EnvironmentException;
import gameplay.TimeObserver;
import weapon.Weapon;

public class Environment implements EnvironmentSubject
{
	private Cell[][] cells;
	private int rows, columns, size, highX, highY;
	private static Environment theWorld = null;
	ArrayList<EnvironmentObserver> theObservers = new ArrayList<EnvironmentObserver>();
	
	
	
	/**
	 * @
	 * @param creates cells array of 1 x 1 
	 */
	private Environment()
	{
		cells = new Cell[1][1];
		size = 1;
		cells[0][0] = new Cell();
	}
	/**
	 * privated for singleton pattern
	 * @param creates cells array defined by user
	 * @param y row amount
	 * @param x column amount
	 */
	private Environment(int y, int x)
	{
		this.rows = y;
		this.columns = x;
		cells = new Cell[y][x];
		size = y * x;
		for(int a = 0; a < y; a++)
		{
			for(int b = 0; b < x; b++)
			{
				cells[a][b] = new Cell();
			}
		}
	}
	
	/**
	 * method to create one instance of environment based on singleton pattern
	 * @param y
	 * @param x
	 * @return
	 */
	public static Environment getInstance(int y, int x)
	{
		if (theWorld == null)
			if (theWorld == null)
				theWorld = new Environment(y, x);

		return theWorld;
	}
	/**
	 * @param y row location
	 * @param x column location
	 * @return LifeForm in the cell, null if empty or out of bounds
	 */
	public LifeForm getLifeForm(int y, int x)
	{
		if (((y+1) * (x+1)) <= size && y >= 0 && x >= 0)
		{
			return cells[y][x].getLifeForm();
		}
		else 
			return null;
	}

	/**
	 * 
	 * @param y row location
	 * @param x column location
	 * @param entity LifeForm being added
	 * @return true if in bounds, false if not
	 */
	public boolean addLifeForm(int y, int x, LifeForm entity)
	{
		//System.out.println("Size: " + size);
		if (((y+1) * (x+1)) <= size && y >= 0 && x >= 0 && (cells[y][x].getLifeForm() == null))
		{
			cells[y][x].addLifeForm(entity);
			entity.setLocation(y,x);
			environmentChanged();
			return true;
		}
		else 
			return false;
	}

	/**
	 * 
	 * @param y row location
	 * @param x column location
	 * @return true if in bounds, false if not
	 */
	public boolean removeLifeForm(int y, int x)
	{
		if (((y+1) * (x+1)) <= size && y >= 0 && x >= 0)
		{
			cells[y][x].removeLifeForm();
			environmentChanged();
			return true;
		}
		else 
			return false;
	}
	
	/**
	 * method to get size of environment
	 * @return the size of the environment
	 */
	public int getSize()
	{
		return size;
	}
	/***************************
	 * Lab 5 Starts here!
	 ***************************/

	/**
	 * method to add a weapon to a cell
	 * @param weapon the weapon being added to a cell
	 * @param index the location for the weapon
	 * @param x the row 
	 * @param y the column
	 */
	public void addWeaponToCell(Weapon weapon,int index, int x, int y)
	{
		if (((y+1) * (x+1)) <= size && y >= 0 && x >= 0 && ((cells[y][x].weapon1 == null) || (cells[y][x].weapon2 == null)))
			cells[y][x].addWeaponToCell(weapon, index);		
		environmentChanged();
	}
	
	public boolean addWeaponToCellSimulator(int x, int y, Weapon weapon)
	{
		boolean result = false;
		if (cells[y][x].getWeapon(0) == null)
		{
			cells[y][x].addWeaponToCell(weapon, 0);
			result = true;
		}
		else
			result = false;
			
		return result;
	}

	/**
	 * method for a lifeform to exchange its weapon with on in the cell
	 * @param weapon the weapon that hte lifeform is giving up
	 * @param index the slot it wants a weapon from
	 * @param x the column
	 * @param y the row
	 * @return the weapon from the cell
	 */
	public Weapon exchangeWeapon(Weapon weapon, int index, int x, int y)
	{
		Weapon temp=null;
		if (((y+1) * (x+1)) <= size && y >= 0 && x >= 0)
		temp= cells[y][x].exchangeWeapon(weapon, index);
		environmentChanged();
		return temp;
	}
	
	/**
	 * method to remove a weapon to a cell in the environment
	 * @param index which slot, of 2, to put the weapon in
	 * @param x which column
	 * @param y which row
	 * @return weapon being removed
	 */
	public Weapon removeWeaponFromCell(int index, int x, int y)
	{
		Weapon temp=null;
		if (((y+1) * (x+1)) <= size && y >= 0 && x >= 0)
		temp = cells[y][x].removeWeaponFromCell(index);
		environmentChanged();
		return temp;
	}
	public Weapon getWeapon(int index, int x, int y)
	{
		return cells[y][x].getWeapon(index);
	}
	/**
	 * method to destroy the current environment instance
	 */
	public static void clearBoard()
	{
		theWorld=null;
	}
	
	
	/**
	 * method used to compute the range between two life forms when given two lifeforms
	 * @param thing1 one of the lifeforms
	 * @param thing2 the other lifeform
	 * @return the range between the two lifeforms
	 * @throws EnvironmentException thrown if one of the lifeforms is not actually placed in the environment. They are marked as placed
	 * 								only if they have successfully been added to a cell.
	 */
	public int getDistance(LifeForm thing1, LifeForm thing2) throws EnvironmentException
	{
		int distance;
		distance = 0;
		
		if ( thing1.getPlaced() == true && thing2.getPlaced() == true)
		{
			//System.out.println("X's:" + thing1.getX() + thing2.getX() + "Y's:" + thing1.getY() +  thing2.getY());
			//System.out.println("Abs X: " + Math.abs(thing1.getX() - thing2.getX()) + "Abs Y:" + Math.abs(thing1.getY() - thing2.getY()));
			//System.out.println("X's * 5 squared:" + (Math.pow(5*Math.abs(thing1.getX() - thing2.getX()),2)) + "Y's * 5 squared:" + (Math.pow(5*Math.abs(thing1.getY() - thing2.getY()),2)));
			distance = (int)Math.sqrt((Math.pow(5*Math.abs(thing1.getX() - thing2.getX()),2))+(Math.pow(5*Math.abs(thing1.getY() - thing2.getY()),2)));
		}
		else
			throw new EnvironmentException();
		return distance;
	}
	
	
	/*
	 * Lab6 starts here
	 * 
	 * 
	 */
	
	
	/*
	 * add an environment observer
	 */
	public void addEnvironmentObserver(EnvironmentObserver observer)
	{
		observer.update();
		theObservers.add(observer);
	}
	
	/**
	 * remove an environment observer
	 */
	public void removeEnvironmentObserver(EnvironmentObserver observer)
	{
		theObservers.remove(observer);
	}
	
	/**
	 * method to tell all observers the environment changed
	 */
	public void environmentChanged()
	{	
		if (!(theObservers.isEmpty()));
		{
			for (int i = 0; i < theObservers.size(); i++)
			{
				(theObservers.get(i)).update();	
			}
		}
	}
	
	/**
	 * 
	 * method to move a lifeform maxSpeed to the farthest empty cell
	 * @param name the lifeform being moved
	 * @return was it successful
	 */
	public boolean moveLifeForm(LifeForm name)
	{
		
		int[] starting = new int[2];
		starting[0] = name.getX();
		starting[1] = name.getY();
		int distance = name.getMoveSpeed();
		boolean stuff = false;
		
		Facing direction = name.getDirection();	// sets the direction to be moved
		//System.out.println("got to move method");
		if (distance <= name.getMoves())	// if desired move distance is possible
		{	
			//System.out.println("got to first if");
			if (direction == Facing.North)
			{
				//System.out.println("got to second if");
				for(int i = distance; i > 0 && !stuff; i--)
				{
					//System.out.println("loop");
					if (((starting[1] - i) >= 0) && (getLifeForm(starting[1] - i, starting[0]) == null))
					{
						//System.out.println(getLifeForm(starting[0], starting[1] - i));
						//System.out.println("*_*_" + name.getX() + "||" + name.getY());
						//System.out.println("found null cell for lifeform");
						removeLifeForm(starting[1], starting[0]);
						addLifeForm(starting[1] - i, starting[0], name);
						name.setMoves(i);	// reduce movesLeft by the number of spaces moved
						stuff = true;	// moved north
					}
				}
			}
			else if (direction == Facing.East)
			{
				for(int i = distance; i > 0 && !stuff; i--)
				{
					if (((starting[0] + i) < columns) && (getLifeForm(starting[1], starting[0] + i) == null))
					{
						removeLifeForm(starting[1], starting[0]);
						addLifeForm(starting[1], starting[0] + i,  name);
						name.setMoves(i);	// reduce movesLeft by the number of spaces moved
						stuff = true;	// moved east
					}
				}
			}
			else if (direction == Facing.South)
			{
				for(int i = distance; i > 0 && !stuff; i--)
				{
					if (((starting[1] + i) < rows) && (getLifeForm(starting[1]+ i, starting[0] ) == null))
					{
						removeLifeForm(starting[1], starting[0]);
						addLifeForm( starting[1] + i, starting[0],  name);
						name.setMoves(i);	// reduce movesLeft by the number of spaces moved
						stuff = true;	// moved south
					}
				}
			}
			else if (direction == Facing.West)
			{
				for(int i = distance; i > 0 && !stuff; i--)
				{
					if (((starting[0] - i) >= 0) && (getLifeForm(starting[1], starting[0] - i) == null))
					{
						removeLifeForm(starting[1], starting[0]);
						addLifeForm( starting[1], starting[0] - i, name);
						name.setMoves(i);	// reduce movesLeft by the number of spaces moved
						stuff = true;	// moved west
					}
				}
			}
		}

		environmentChanged();
		return stuff;	// if desired move distance greater than movesLeft
		
	}
	
	/**
	 * method to set what cell is highlighted
	 * @param x cord of highlighted
	 * @param y cord of highlighted
	 */
	public void setHighlightedCell(int x, int y)
	{
		highX = x;
		highY = y;
		environmentChanged();
	}
	
	/**
	 * method to return number of rows
	 * @return rows
	 */
	public int getRows()
	{
		return rows;
	}
	
	/**
	 * method to return the number of columns
	 * @return columns
	 */
	public int getColumns()
	{
		return columns;
	}
	
	/**
	 * method to get highlighted cell x cord.
	 * @return x cord
	 */
	public int getHighlightedX()
	{
		return highX;
	}
	
	/**
	 * method to get the highlighted cells y cord.
	 * @return y cord.
	 */
	public int getHighlightedY()
	{	
		return highY;
	}
	
	/**
	 * method for objects to interface and move a lifeform without having an instance of environment
	 * @param thing the lifeform being moved
	 */
	public static void move(LifeForm thing)
	{
		theWorld.moveLifeForm(thing);
	}
	
	/**
	 * method to find a lifeform if one exists in front of an attacking lifeform
	 * @param lifeFormOne the attacking lifeform
	 * @throws EnvironmentException somthing broke
	 */
	public void findTarget(LifeForm lifeFormOne) throws EnvironmentException
	{
		int x = lifeFormOne.getX();
		int y = lifeFormOne.getY();
		
		LifeForm lifeFormTwo = null;
		
		//determine which direction the lifeform is facing and look for another lifeform to attack in that direction
		if (lifeFormOne.getDirection() == Facing.East)
		{
			while(x < columns && lifeFormTwo == null)
			{
				if (cells[y][x].getLifeForm() != null && cells[y][x].getLifeForm() != lifeFormOne)
					lifeFormTwo = cells[y][x].getLifeForm();
				x++;
			}
		}
		if (lifeFormOne.getDirection() == Facing.West)
		{
			while(x >= 0  && lifeFormTwo == null)
			{
				if (cells[y][x].getLifeForm() != null && cells[y][x].getLifeForm() != lifeFormOne)
					lifeFormTwo = cells[y][x].getLifeForm();
				x--;
			}
		}
		if (lifeFormOne.getDirection() == Facing.North)
		{
			while(y >= 0  && lifeFormTwo == null)
			{
				if (cells[y][x].getLifeForm() != null && cells[y][x].getLifeForm() != lifeFormOne)
					lifeFormTwo = cells[y][x].getLifeForm();
				y--;
			}
		}
		if (lifeFormOne.getDirection() == Facing.South)
		{
			while(y < rows  && lifeFormTwo == null)
			{
				if (cells[y][x].getLifeForm() != null && cells[y][x].getLifeForm() != lifeFormOne)
					lifeFormTwo = cells[y][x].getLifeForm();
				y++;
			}
		}
		
		//if we found a lifeform have it take a hit from the attacking lifeform with damage based on the distacne between them
		if (lifeFormTwo != null)
			lifeFormTwo.takeHit(lifeFormOne.getAttack(theWorld.getDistance(lifeFormOne, lifeFormTwo)));
		else // attack anyway because the button was pressed, which will exhaust ammo if the life form has a gun
			lifeFormOne.getAttack(99);
	}
	
	/**
	 * Method that allows objects to interface with the environment without containing an instance in order to determine and attack
	 * @param lifeFormOne the lifeform doing the attacking
	 * @throws EnvironmentException something screwed up
	 */
	public static void attack(LifeForm lifeFormOne) throws EnvironmentException
	{
		//call find target in the singleton environment instance
		theWorld.findTarget(lifeFormOne);
	}

	/**
	 * method to find a lifeform if one exists in front of an attacking lifeform
	 * @param lifeFormOne the attacking lifeform
	 * @throws EnvironmentException somthing broke
	 */
	public boolean findTargetAI(LifeForm lifeFormOne) throws EnvironmentException
	{
		int x = lifeFormOne.getX();
		int y = lifeFormOne.getY();
		
		LifeForm lifeFormTwo = null;
		
		//determine which direction the lifeform is facing and look for another lifeform to attack in that direction
		if (lifeFormOne.getDirection() == Facing.East)
		{
			while(x < columns && lifeFormTwo == null)
			{
				if (cells[y][x].getLifeForm() != null && cells[y][x].getLifeForm() != lifeFormOne)
					lifeFormTwo = cells[y][x].getLifeForm();
				x++;
			}
		}
		if (lifeFormOne.getDirection() == Facing.West)
		{
			while(x >= 0  && lifeFormTwo == null)
			{
				if (cells[y][x].getLifeForm() != null && cells[y][x].getLifeForm() != lifeFormOne)
					lifeFormTwo = cells[y][x].getLifeForm();
				x--;
			}
		}
		if (lifeFormOne.getDirection() == Facing.North)
		{
			while(y >= 0  && lifeFormTwo == null)
			{
				if (cells[y][x].getLifeForm() != null && cells[y][x].getLifeForm() != lifeFormOne)
					lifeFormTwo = cells[y][x].getLifeForm();
				y--;
			}
		}
		if (lifeFormOne.getDirection() == Facing.South)
		{
			while(y < rows  && lifeFormTwo == null)
			{
				if (cells[y][x].getLifeForm() != null && cells[y][x].getLifeForm() != lifeFormOne)
					lifeFormTwo = cells[y][x].getLifeForm();
				y++;
			}
		}
		Boolean result = false;
		//if we found a lifeform have it take a hit from the attacking lifeform with damage based on the distacne between them
		if (lifeFormTwo != null) 
		{
			if (theWorld.getDistance(lifeFormOne, lifeFormTwo) <= 5 || lifeFormOne.getWeapon() != null && lifeFormOne.getWeapon().getMaxRange() > theWorld.getDistance(lifeFormOne, lifeFormTwo))
			{
				lifeFormTwo.takeHit(lifeFormOne.getAttack(theWorld.getDistance(lifeFormOne, lifeFormTwo)));
				result = true;
			}
		}
		
		else // attack anyway because the button was pressed, which will exhaust ammo if the life form has a gun
			result = false;
		
		return result;
	}
		
}
