package environment;

public interface EnvironmentObserver 
{
	//method in observers that will be used to update the GUIs after changes
	public void update();
	//
}


