package weapon;
/**
 * @Bradley Eddowes & Isaiah English & Kanza Amin Test
 */
public class ChainGun extends GenericWeapon
{
	//constructor if special name not given
	public ChainGun()
	{
		super("Chain Gun", 15, 30, 4, 40);
	}
	//constructor for it a special name is given
	public ChainGun(String name)
	{
		super(name, 15, 30, 4, 40);
	}
	//default damage calculator
	public int fire(int distance)
	{
		int damage = 0;
		if (getActualAmmo() > 0 && getShotsLeft() > 0)
		{
			if (distance <= getMaxRange())
			{
				damage = (int)((double)((double)this.getBaseDamage()) * (double)((double)distance / (double)this.getMaxRange()));
			}
			
			setActualAmmo(-1);
			setShotsLeft(-1);
		}
		return damage;
	}
}