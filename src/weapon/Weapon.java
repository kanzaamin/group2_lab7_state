package weapon;
/**
 * @Bradley Eddowes & Isaiah English & Kanza Amin Test
 * 
 * this is the interface for generic weapons to interface to
 */
import gameplay.TimeObserver;

public interface Weapon extends TimeObserver
{
	//methods that needed exposed to attachments for the concrete attachments to function
	//main method for weapons to calculate damage and for attachments to augment
	public int fire(int distance);
	public int getMaxRange();
	public int getActualAmmo();
	public int getShotsLeft();
	public int getMaxAmmo();
	public int getNumAttachments();
	public String getName();
	public void setActualAmmo(int deduction);
	public void setShotsLeft(int deduction);
	public void updateName(String addition);
	public void updateNumAttachments(int addition);
	public void reload();
}
