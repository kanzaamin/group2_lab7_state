package weapon;
/**
 * @Bradley Eddowes & Isaiah English & Kanza Amin Test
 */
public class PowerBooster extends Attachment
{
	public Weapon baseWeapon;
	
	public PowerBooster(Weapon weapon) 
	{
		super(weapon);
		baseWeapon = weapon;
		//update base weapons name and number of attachments
		updateName(" + Power Booster");
		updateNumAttachments(1);
	}
	
	// overridden fire method to enhance base weapons effect 
	public int fire(int distance)
	{
		int damage = 0;
		if (getActualAmmo() > 0 && getShotsLeft() > 0)
		{
			if (distance <= getMaxRange())
			{
				damage = (int)(((double)(1 + (double)(getActualAmmo()/(double)getMaxAmmo())) * (double)baseWeapon.fire(distance)));
			}
			else
			{
				setActualAmmo(-1);
				setShotsLeft(-1);
			}
		}
		return damage;
	}
}