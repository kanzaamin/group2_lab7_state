package weapon;
/**
 * @Bradley Eddowes & Isaiah English & Kanza Amin Test
 */
public class Pistol extends GenericWeapon
{
	//constructor for no special name
	public Pistol()
	{
		super("Pistol", 10, 25, 2, 10);
	}
	//constructor if special name is given
	public Pistol(String name)
	{
		super(name, 10, 25, 2, 10);
	}
	//default damage calculator 
	public int fire(int distance)
	{
		int damage = 0;
		if (getActualAmmo() > 0 && getShotsLeft() > 0)
		{
			if (distance <= getMaxRange())
			{
				setActualAmmo(-1);
				setShotsLeft(-1);
				damage = (int)((double)(this.getBaseDamage()) * (double)((double)this.getMaxRange() - distance + 5)/(double)this.getMaxRange());
			}
			else
			{
				setActualAmmo(-1);
				setShotsLeft(-1);
				
			}
		}
		return damage;
	}
}
