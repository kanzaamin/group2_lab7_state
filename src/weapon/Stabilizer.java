package weapon;
/**
 * @Bradley Eddowes & Isaiah English & Kanza Amin Test
 */
public class Stabilizer extends Attachment
{
	Weapon baseWeapon;
	
	public Stabilizer(Weapon weapon) 
	{
		super(weapon);
		baseWeapon = weapon;
		//update base weapon name and num attachments 
		updateName(" + Stabilizer");
		updateNumAttachments(1);
	}
	
	//enhanced damage calculator
	public int fire(int distance)
	{
		int damage = 0;
		if (getActualAmmo() > 0 && getShotsLeft() > 0)
		{
			if (distance <= baseWeapon.getMaxRange())
			{
				damage = (int)Math.floor((double)baseWeapon.fire(distance) * 1.25);
				if (getActualAmmo() == 0)
					baseWeapon.setActualAmmo(baseWeapon.getMaxAmmo());
			}
			else
			{
				setActualAmmo(-1);
				setShotsLeft(-1);
				if (getActualAmmo() == 0)
					baseWeapon.setActualAmmo(baseWeapon.getMaxAmmo());
			}
		}
		return damage;
	}
	
}
