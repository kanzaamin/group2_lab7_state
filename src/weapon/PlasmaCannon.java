package weapon;
/**
 * @Bradley Eddowes & Isaiah English & Kanza Amin Test
 */
public class PlasmaCannon extends GenericWeapon
{
	//constructor if no special name
	public PlasmaCannon()
	{
		super("Plasma Cannon", 50, 20, 1, 4);
	}
	//Constructor if special name is given
	public PlasmaCannon(String name)
	{
		super(name, 50, 20, 1, 4);
	}
	//basic damage calculator
	public int fire(int distance)
	{
		int damage = 0;
		if (getActualAmmo() > 0 && getShotsLeft() > 0)
		{
			if (distance <= getMaxRange())
			{
				damage = (int)((double)(this.getBaseDamage()) * (double)((double)this.getActualAmmo() / (double)this.getMaxAmmo()));
			}
			setActualAmmo(-1);
			setShotsLeft(-1);
		}
		return damage;
	}
}