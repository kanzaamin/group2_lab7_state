package weapon;
/**
 * @Bradley Eddowes & Isaiah English & Kanza Amin Test
 * 
 * used to store all the methods that the guns may need so that code is not copied between weapons and because it can not be stored in the interface
 */
public abstract class GenericWeapon implements Weapon
{
	private int baseDamage, maxRange, rateOfFire, shotsLeft, maxAmmo, actualAmmo, numAttachments;
	private String name;
	
	/**
	 * 
	 * @param name name of the weapon being created
	 * @param baseDamage the basic damage of the weapon
	 * @param maxRange how far it can shoot 
	 * @param rateOfFire how many times per round it can shoot
	 * @param maxAmmo the amount of ammo it has after reloading
	 */
	public GenericWeapon(String name, int baseDamage, int maxRange, int rateOfFire, int maxAmmo)
	{
		this.name = name;
		this.baseDamage = baseDamage;
		this.maxRange = maxRange;
		this.rateOfFire = rateOfFire;
		shotsLeft = rateOfFire;
		this.maxAmmo = maxAmmo;
		actualAmmo = maxAmmo;
	}
	
	//methods below are self explanatory
	public int getBaseDamage()
	{
		return baseDamage;
	}
	public int getMaxRange()
	{
		return maxRange;
	}
	public int getRateOfFire()
	{
		return rateOfFire;
	}
	public int getShotsLeft()
	{
		return shotsLeft;
	}
	public int getMaxAmmo()
	{
		return maxAmmo;
	}
	public int getActualAmmo()
	{
		return actualAmmo;
	}
	public int getNumAttachments()
	{
		return numAttachments;
	}
	public String getName()
	{
		return name;
	}
	public void setActualAmmo(int deduction)
	{
		actualAmmo += deduction;
	}
	public void setShotsLeft(int deduction)
	{
		shotsLeft += deduction;
	}
	public void updateName(String addition)
	{
		name = name + addition;
	}
	public void updateNumAttachments(int addition)
	{
		numAttachments += addition;
	}
	public void reload()
	{
		actualAmmo = maxAmmo;
	}
	// when it becomes a new round, the shots available to take are reset
	public void updateTime(int time) 
	{
		shotsLeft = rateOfFire;
	}
	public int getTime() 
	{
		return 0;
	}
}
