package weapon;
/**
 * 
 * @Bradley Eddowes & Isaiah English & Kanza Amin Test
 *
 */

public class Scope extends Attachment
{
	private Weapon baseWeapon;
	
	/**
	 * 
	 * @param weapon the weapon being given to scope for scope to modify
	 */
	public Scope(Weapon weapon) 
	{
		super(weapon);
		//keep track of the base weapon
		baseWeapon = weapon;
		//update name and number of attachments
		baseWeapon.updateName(" + Scope");
		baseWeapon.updateNumAttachments(1);
	}
	
	/**
	 * @param distance the range of the anemy being attacked
	 * 
	 * Overridden fire method to enhance the base weapons effects
	 */
	public int fire(int distance)
	{
		int damage = 0;
		if (getActualAmmo() > 0 && getShotsLeft() > 0)
		{
			if (baseWeapon.getMaxRange() < distance && distance <= this.getMaxRange())
			{
				damage = 5 + baseWeapon.fire(baseWeapon.getMaxRange());
			}
			else if (distance < baseWeapon.getMaxRange())
			{
				return (int)((double)baseWeapon.fire(distance) * (double)(1 + (((double)getMaxRange() - (double)distance)/(double)getMaxRange())));
			}
			else
			{
				setActualAmmo(-1);
				setShotsLeft(-1);
			}
		}
		return damage;
	}
	
	// overridden range fetcher because the scope changes the base weapons max range
	public int getMaxRange()
	{
		return (baseWeapon.getMaxRange() + 10);
	}
}