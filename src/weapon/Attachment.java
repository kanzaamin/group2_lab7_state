package weapon;

/**
 * 
 * @Bradley Eddowes & Isaiah English & Kanza Amin Test
 *
 */

public class Attachment implements Weapon
{
	protected Weapon baseWeapon;
	
	/**
	 * 
	 * @param weapon the base weapon that attachment will help keep track of
	 */
	public Attachment(Weapon weapon)
	{
		baseWeapon = weapon;
	}

	@Override
	public void updateTime(int time) 
	{
		baseWeapon.updateTime(time);
	}


	@Override
	public int getTime() 
	{
		return baseWeapon.getTime();
	}


	@Override
	public int fire(int distance) 
	{
		return baseWeapon.fire(distance);
	}


	@Override
	public int getMaxRange() 
	{
		return baseWeapon.getMaxRange();
	}


	@Override
	public int getActualAmmo() 
	{
		return baseWeapon.getActualAmmo();
	}


	@Override
	public int getShotsLeft() 
	{
		return baseWeapon.getShotsLeft();
	}


	@Override
	public int getMaxAmmo() 
	{
		return baseWeapon.getMaxAmmo();
	}


	@Override
	public void setActualAmmo(int deduction) 
	{
		baseWeapon.setActualAmmo(deduction);
	}


	@Override
	public void setShotsLeft(int deduction) 
	{
		baseWeapon.setShotsLeft(deduction);
	}


	@Override
	public void updateName(String addition) 
	{
		baseWeapon.updateName(addition);
	}


	@Override
	public void updateNumAttachments(int addition) 
	{
		baseWeapon.updateNumAttachments(addition);
	}


	@Override
	public void reload() 
	{
		baseWeapon.reload();
	}


	@Override
	public String getName()
	{
		return baseWeapon.getName();
	}


	@Override
	public int getNumAttachments() 
	{
		return baseWeapon.getNumAttachments();
	}
}
