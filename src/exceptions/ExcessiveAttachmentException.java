package exceptions;

public class ExcessiveAttachmentException extends Exception 
{
	public ExcessiveAttachmentException()
	{
		super("Attempted to attach too many weapon attachments");
	}
}
