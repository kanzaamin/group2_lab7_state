package recovery;
/**
 * for recovering a fractional amount of health
 * @author Bradley Eddowes
 *
 */
public class RecoveryFractional implements RecoveryBehavior
{
	private double percentRecovery;
	/**
	 * 
	 * @param percent percent of health to be recovered
	 */
	public RecoveryFractional(double percent)
	{
		percentRecovery = percent / 100;
	}
	
	/**
	 * @param current life
	 * @param max life
	 * @return	new current life points
	 */
	public int calculateRecovery(int currentLife, int maxLife)
	{
		if (currentLife < maxLife && currentLife > 0)
		{
			double addLife = Math.ceil(currentLife * percentRecovery);
			currentLife += addLife;
			
			if (currentLife > maxLife)
				currentLife = maxLife;
		}
		return currentLife;
	}
}
