package recovery;
/**
 * recovery interface for other implemented recovery methods
 * @author Bradley Eddowes
 *
 */
public interface RecoveryBehavior 
{
	public int calculateRecovery(int currentLife, int maxLife);
}
