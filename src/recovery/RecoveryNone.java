package recovery;
/**
 * no recovery 
 * @author Bradley Eddowes
 *
 */
public class RecoveryNone implements RecoveryBehavior
{
	/**
	 *  @param current life to be returned (no healing done)
	 *  @param max life (does not matter)
	 */
	public int calculateRecovery(int currentLife, int maxLife)
	{
		return currentLife;
	}
}
