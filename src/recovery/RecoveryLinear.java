package recovery;
/**
 * for recovering a set amount of points
 * @author Bradley Eddowes
 *
 */
public class RecoveryLinear implements RecoveryBehavior
{
	private int recoveryStep;
	/**
	 * 
	 * @param step amount of health to be recovered each time
	 */
	public RecoveryLinear(int step)
	{
		recoveryStep = step;
	}
	
	/**
	 * @param curren life
	 * @param max life
	 * @return new current life points
	 */
	public int calculateRecovery(int currentLife, int maxLife)
	{
		if (currentLife < maxLife && currentLife > 0)
		{
			currentLife += recoveryStep;
			if (currentLife > maxLife)
				currentLife = maxLife;
		}
		return currentLife;
	}
}
