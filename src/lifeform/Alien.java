package lifeform;
/*
 * Brad Eddowes
 * 
 * This is the Alien class
 */
import exceptions.negativeRecoveryRate;

/**
 * Brad Eddowes
 * 
 * This is the Alien class
 */

import recovery.RecoveryBehavior;
import recovery.RecoveryNone;

public class Alien extends LifeForm
{
	RecoveryBehavior recoveryBehavior;
	private int recoveryRate;
	private int maxLifePoints;
	private int currentRound;
	private int recoveryCount;
	private int moveSpeed = 2;
	/**
	 * 
	 * @param life max and current life points
	 * @throws negativeRecoveryRate 
	 */
	public Alien(int life) 
	{
		this("Default Name", life, new RecoveryNone(), 10, 1000);
	}
	/**
	 * 
	 * @param name aliens name
	 * @param life aliens life amount
	 * @throws negativeRecoveryRate 
	 */
	public Alien(String name, int life) 
	{
		this(name, life, new RecoveryNone(), 10, 1000);
	}
	
	/**
	 * 
	 * @param name aliens name
	 * @param life aliens max and current life points 
	 * @param rb the chosen recovery behavior
	 * @throws negativeRecoveryRate 
	 */
	public Alien(String name, int life, RecoveryBehavior rb) 
	{
		this(name, life, rb, 10, 1000);
	}
	
	/**
	 * 
	 * @param name alien name	
	 * @param life current and max life points 
	 * @param rb chosen recovery behavior for alien
	 * @param strength attack strength
	 * @param RR recovery rate of alien
	 */
	public Alien(String name, int life, RecoveryBehavior rb, int strength, int RR) 
	{
		super(name, life, strength);
		maxLifePoints = life;
		recoveryBehavior = rb;
		recoveryCount = 0;
		setMoveSpeed(moveSpeed);
		
		try
		{
			if (RR > 0)
				recoveryRate = RR;
			else if (RR == 0)
				recoveryRate = RR;
			else if (RR < 0)
				throw new negativeRecoveryRate();
		}
		catch (negativeRecoveryRate e) 
		{
				e.printStackTrace();
		}
	}

	//method for recovery cycle. 
	public void recover()
	{
		setCurrentLifePoints(recoveryBehavior.calculateRecovery(getLifePoints(), maxLifePoints));
	}
	/**
	 * 
	 * @param life new current life points
	 */
	public void setCurrentLifePoints(int life)
	{
		currentLifePoints = life;
	}
	/**
	 * @param round the current game round
	 * 
	 * method to override LifeForm's updateTime to handle recovery
	 */
	public void updateTime(int round)
	{
		currentRound = round;
		super.updateTime(round);
		//make sure the alien has a recovery rate
		if (recoveryRate != 0)
		{
			if (recoveryCount == recoveryRate)
			{
				recover();
				recoveryCount = 0;
			}
			else if (recoveryCount < 0)
				recoveryCount = 1;
			else
				recoveryCount++;
		}	
	}
	
	public void setRecoveryRate(int RR) throws negativeRecoveryRate
	{
		recoveryCount = 1;

			if (RR > 0)
				recoveryRate = RR;
			else if (RR == 0)
				recoveryRate = RR;
			else if (RR < 0)
				throw new negativeRecoveryRate();
	}
	public RecoveryBehavior getRecoveryRate()
	{
		return recoveryBehavior;
	}
	
	public double getRecoveryNumber()
	{
		return recoveryRate;
	}
}
