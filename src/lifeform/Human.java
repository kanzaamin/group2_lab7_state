package lifeform;
/**
 * 
 * @author Bradley Eddowes
 *
 * the human class
 */
public class Human extends LifeForm
{
	private int armorPoints;
	private int moveSpeed = 3;
	/**
	 * @param name of human
	 * @param amount of life points
	 * @param amount of armor points
	 * @param strength of attack
	 */
	public Human(String name, int life, int armor)
	{
		this(name, life, armor, 5);
		setMoveSpeed(moveSpeed);
	}
	
	/**
	 * 
	 * @param name humans name
	 * @param life humans total life points
	 * @param armor the amount of armor the human has
	 * @param strength of the humans attack
	 */
	public Human(String name, int life, int armor, int strength)
	{
		super(name, life, strength);
		
		if ( armor > 0)
			armorPoints = armor;
		else 
			armorPoints = 0;
	}
	
	public void takeHit(int damage)
	{
		if (damage <= armorPoints)
			super.takeHit(0);
		else
		{
			damage -= armorPoints;
			super.takeHit(damage);
		}
		setMoveSpeed(moveSpeed);
	}
	/**
	 * @return the current armor points
	 */
	public int getArmorPoints()
	{
		return armorPoints;
	}
	/**
	 * @param new amount of armor
	 */
	public void setArmorPoints(int armor)
	{
		if (armor > 0)
			armorPoints = armor;
		else
			armorPoints = 0;
	}
}
