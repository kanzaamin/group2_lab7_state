/**
 * Keeps track of the information associated with a simple life form.
 * also provides the functionality related to the life form.
 * @bradley Eddowes Test
 *
 */
package lifeform;

import gameplay.TimeObserver;
import weapon.Weapon;

/**
 * 
 * @author Bradley Eddowes Plus addtions by Lab 4 members Isaiah English & Kanza Amin Test
 *
 */
public abstract class LifeForm implements TimeObserver
{
	private String myName;
	private int attackStrength;
	private int gameTime;
	private int xCoord, yCoord;
	private boolean beenPlaced;
	private Weapon weapon;
	protected int currentLifePoints;
	private int maxSpeed;
	private Facing direction;
	private int moves;
/**
 * Create an instance
 * @param name the name of the life form
 * @param points the current starting life points of the life form
 */
		public LifeForm(String name, int points, int strength)
		{
			myName = name;
			currentLifePoints = points;
			attackStrength = strength;
			beenPlaced = false;
			direction = Facing.North;
		}
		/**
		 * sets the life forms weapon if it does not poses one alread
		 * 
		 * @param the weapon being given to life form
		 */
		public void setWeapon(Weapon weapon)
		{
			if(this.weapon == null)
			{
				this.weapon = weapon;
			}
		}
		
		public Weapon getWeapon()
		{
			return weapon;
		}
		/*
		 * removes the life forms weapon
		 */
		public Weapon removeWeapon()
		{
			Weapon temp = weapon;
			weapon = null;
			return temp;
		}
		
		public void reload()
		{
			if (weapon != null)
				weapon.reload();
		}
		/**
		 * 
		 * @return the strength of attack the life form currently has
		 */
		public int getStrength()
		{
			if (currentLifePoints > 0)
				return attackStrength;
			else
				return 0;
		}
		
		/**
		 * calculated the damage done by life form taking into consideration range and weapon conditions
		 * 
		 * @param the range of the enemy being attacked
		 */
		public int getAttack(int range)
		{
			int damage = 0;
			
			if (currentLifePoints > 0)
			{
				if (weapon != null)
				{
					if (weapon.getActualAmmo() != 0 && weapon.getShotsLeft() != 0)
					{
						damage = weapon.fire(range);
					}
					else if (range <= 5)
						damage = attackStrength; 
				}
				else if (range <= 5)
					damage = attackStrength;
			}
			return damage;
		}
/**
 * 	@return the name of the life form.
 */
		public String getName()
		{
			return myName;
		}
/**
 * @return the amount of current life points the life form has.
 */
		public int getLifePoints()
		{
			return currentLifePoints;
		}

		/**
		 * 
		 * @param damage the damage being done to the life form
		 */
		public void takeHit(int damage)
		{
			if (damage > 0)
			{
				if (currentLifePoints >= damage)
					currentLifePoints -= damage;
				else
					currentLifePoints = 0;
			}
		}
		
		public void updateTime(int time)
		{
			gameTime = time;
			moves = maxSpeed;
			// update the weapons time if it has one, for testing
			if (weapon != null)
				weapon.updateTime(time);
		}
		
		
		public int getTime()
		{
			return gameTime;
		}
		/**
		 * sets the location of the lifeform, called when the lifeform is placed or moved in the environment
		 * @param y index location
		 * @param x index location
		 */
		public void setLocation(int y, int x) 
		{
			xCoord = x;
			yCoord = y;
		}
		/**
		 * return the lifeforms x index
		 * @return
		 */
		public int getX()
		{
			return xCoord;
		}
		/**
		 * returns the lifeforms y index
		 * @return
		 */
		public int getY()
		{
			return yCoord;
		}
		
		/**
		 * tells the lifeform it has been placed
		 * @param x
		 */
		public void setPlaced(Boolean x)
		{
			beenPlaced = x;
		}
		
		/**
		 * the lifeform knows it has been placed
		 * @return its status 
		 */
		public Boolean getPlaced()
		{
			return beenPlaced;
		}
		
		/**
		 * sets the max move speed
		 * @param speed max move speed
		 */
		public void setMoveSpeed(int speed)
		{
			maxSpeed = speed;
			moves = maxSpeed;
		}
		
		/**
		 * 
		 * @return the max move speed 
		 */
		public int getMoveSpeed()
		{
			return maxSpeed;
		}
		
		/**
		 * 
		 * @return direction lifeform is facing
		 */
		public Facing getDirection()
		{
			return direction;
		}
	
		
		/**
		 * sets the direction the lifeform is facing
		 * @param direction where to face
		 */
		public void changeDirection(Facing direction)
		{
			
			this.direction = direction;
			if (Facing.North == direction)
			{
				this.direction = direction;
			}
			if (Facing.East == direction)
			{
				this.direction = direction;
			}
			if (Facing.South == direction)
			{
				this.direction = direction;
			}
			if (Facing.West == direction)
			{
				this.direction = direction;
			}
		}
		
		/**
		 * returns the number of moves a lifeform has left
		 * @return remaing moves
		 */
		public int getMoves()
		{
			return moves;
		}
		
		public void setMoves(int distanceMoved)
		{	
		} 
}
