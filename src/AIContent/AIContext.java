package AIContent;

import environment.Environment;
import exceptions.EnvironmentException;
import lifeform.LifeForm;

public class AIContext 
{
	LifeForm lifeform; // the life form the AI will control
	Environment e = Environment.getInstance(5, 5);
	
	ActionState currentState, HasWeaponState, NoWeaponState, DeadState, OutOfAmmoState; //each of the states that the AI will have and the current one it is in
	
	int currentTime; // current round/time
	public AIContext(LifeForm lifeform)
	{
		this.lifeform = lifeform;
		
		HasWeaponState = new HasWeaponState(lifeform, this, e);
		NoWeaponState = new NoWeaponState(lifeform, this, e);
		DeadState = new DeadState(lifeform, this, e); 
		OutOfAmmoState = new OutOfAmmoState(lifeform, this);
		
		if (lifeform.getWeapon() != null)
			currentState = HasWeaponState;
		else
			currentState = NoWeaponState;
	}
	
	//method that will execute currentState.executeAction()
	public void takeTurn(int round)
	{
		currentTime = round;
		
		currentState.executeAction();
	}
	
	public void setNoWeapon()
	{
		currentState = NoWeaponState;
	}
	public void setDeadState()
	{
		currentState = DeadState;
	}
	public void setOutOfAmmo()
	{
		currentState = OutOfAmmoState;
	}
	public void setHasWeapon()
	{
		currentState = HasWeaponState;
	}
	
	public String getState()
	{
		String state = "";
		if(currentState == NoWeaponState )
		{
			state = "No Weapon State";
		}
		else if(currentState == DeadState )
		{
			state = "Dead State";
		}
		else if(currentState == OutOfAmmoState )
		{
			state = "Out Of Ammo State";
		}
		else if(currentState == HasWeaponState )
		{
			state = "Has Weapon State";
		}
		return state;
	}
	
	/**
	 * Changes the State
	 * @param newState the state that AIContext wil be in
	 */
	public void setCurrentState(ActionState newState)
	{
		currentState = newState;
	}
}
