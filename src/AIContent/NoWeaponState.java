package AIContent;

import environment.Environment;
import lifeform.LifeForm;

public class NoWeaponState implements ActionState
{
	LifeForm lifeform;
	AIContext controller;
	Environment e;
	
	//constructor takes in the life form it will control and the aicontext that has it so that this state can changes the aicontext current state 
	public NoWeaponState(LifeForm lifeform, AIContext aiContext, Environment e) 
	{
		this.lifeform = lifeform;
		controller = aiContext;
		this.e = e;
	}

	@Override
	public void executeAction() 
	{
		if(lifeform.getLifePoints() == 0)
		{
			controller.setDeadState();
		}
		else
		{
			if(e.getWeapon(1, lifeform.getX(), lifeform.getY()) != null)
			{
				lifeform.setWeapon(e.getWeapon(1,lifeform.getX(), lifeform.getY()));
				e.removeWeaponFromCell(1, lifeform.getX(), lifeform.getY());
				controller.setHasWeapon();
			}
			else if(e.getWeapon(2, lifeform.getX(), lifeform.getY()) != null)
			{
				lifeform.setWeapon(e.getWeapon(2,lifeform.getX(), lifeform.getY()));
				e.removeWeaponFromCell(1, lifeform.getX(), lifeform.getY());
				controller.setHasWeapon();
			}
			else
			{
				Environment.move(lifeform);
			}
		}
	}

}
