package AIContent;

import environment.Environment;
import exceptions.EnvironmentException;
import lifeform.Facing;
import lifeform.LifeForm;

public class HasWeaponState implements ActionState
{
	LifeForm lifeform;
	AIContext controller;
	Environment e;
	
	//constructor takes in the life form it will control and the aicontext that has it so that this state can changes the aicontext current state 
	public HasWeaponState(LifeForm lifeform, AIContext aiContext, Environment e) 
	{
		this.e = e;
		this.lifeform = lifeform;
		controller = aiContext;
	}

	@Override
	public void executeAction() 
	{
		if(lifeform.getLifePoints() == 0)
		{
			controller.setDeadState();
		} else if(lifeform.getWeapon().getActualAmmo() == 0)
		{
			controller.setOutOfAmmo();
		}
		else
			try {
				if (e.findTargetAI(lifeform) != true)
				{
					//search
					int temp = (int) ((Math.random() * 4) + 0);
					
					if ( temp == 0)
						lifeform.changeDirection(Facing.North);
					else if ( temp == 1)	
						lifeform.changeDirection(Facing.East);
					else if ( temp == 2 )
						lifeform.changeDirection(Facing.South);
					else if ( temp == 3)
						lifeform.changeDirection(Facing.West);
					
					temp = (int) ((Math.random() * 2) + 0);
					
					if (temp == 1)
						Environment.move(lifeform);
				}
			} catch (EnvironmentException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		// TODO Auto-generated method stub
		//do something with the lifeform based on logic listed on P.85 of the lab hand out
		//controller.set*insertstate* executeAction may change the current state of the AIContext based on the logic listed on P.85 of the Lab hand out
		
	}
}
