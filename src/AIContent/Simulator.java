package AIContent;


import java.util.ArrayList;


import environment.Environment;
import exceptions.EnvironmentException;
import gameplay.TimeObserver;
import lifeform.Alien;
import lifeform.Human;
import lifeform.LifeForm;
import recovery.RecoveryBehavior;
import recovery.RecoveryFractional;
import recovery.RecoveryLinear;
import recovery.RecoveryNone;
import weapon.ChainGun;
import weapon.Pistol;
import weapon.PlasmaCannon;
import weapon.PowerBooster;
import weapon.Scope;
import weapon.Stabilizer;
import weapon.Weapon;

public class Simulator implements TimeObserver
{
	ArrayList<AIContext> AI = new ArrayList<AIContext>();
	ArrayList<LifeForm> Things = new ArrayList<LifeForm>();
	ArrayList<Weapon> MoreThings = new ArrayList<Weapon>();
	
	int currentTime;
	LifeForm temp;
	Environment e;
	RecoveryBehavior tempRec;
	Weapon tempWep;
	
	
	public Simulator(int randoms, Environment e) // governor
	{
		this.e = e;

		for (int i = 0; i < randoms; i++)
		{
			if (i%2 == 0)
			{
				temp = new Human("Human AI: "+i,100, randomNumber(9,0));
				Things.add(temp);
				addAI(new AIContext(temp));
			}
			else
			{
				int tempNum = randomNumber(2,0);
						
				if (tempNum == 0)
					tempRec = new RecoveryLinear(randomNumber(20,0));
				else if (tempNum == 1)
					tempRec = new RecoveryFractional(0);
				
				else if (tempNum ==2)
					tempRec = new RecoveryNone();
				
				temp = new Alien("Alien AI: "+i,100,tempRec, 10, randomNumber(5,0));
				
				Things.add(temp);
				addAI(new AIContext(temp));
			}
		}
		
		for ( int i = 0; i < Things.size();i++)
		{
			LifeForm temp = Things.get(i);
			int x, y;
			
			do {
				 x =	(int) (Math.random() * e.getColumns() - 1);
				 y =	(int) (Math.random() * e.getRows() - 1);
			} while (e.addLifeForm(y, x, temp) == false);
		}
		
		//Places weapons randomly
		for (int i = 0; i < randoms; i++)
		{
			int tempNum = randomNumber(2,0);
			
			if (tempNum == 0)
				tempWep = new Pistol();
			else if (tempNum == 1)
				tempWep = new ChainGun();
			else if (tempNum ==2)
				tempWep = new PlasmaCannon();
			
			//get attachments
			tempNum = randomNumber(3,0);
			if (tempNum == 0)
				tempWep = new Scope(tempWep);
			else if (tempNum == 1)
				tempWep = new Stabilizer(tempWep);
			else if (tempNum ==2)
				tempWep = new PowerBooster(tempWep);
			else if(tempNum == 3)
			{}
			MoreThings.add(tempWep);
		}
		
		for ( int i = 0; i < MoreThings.size();i++)
		{
			Weapon tempWep = MoreThings.get(i);
			int x, y;
			
			do {
				 x =	(int) (Math.random() * e.getColumns() - 1);
				 y =	(int) (Math.random() * e.getRows() - 1);
			} while (e.addWeaponToCellSimulator(y, x, tempWep) == false);
		}
		
		//Makes random weapons to give out to lifeforms
		for (int i = 0; i < randoms; i++)
		{
			int tempNum = randomNumber(2,0);
			
			if (tempNum == 0)
				tempWep = new Pistol();
			else if (tempNum == 1)
				tempWep = new ChainGun();
			else if (tempNum ==2)
				tempWep = new PlasmaCannon();
			
			//get attachments
			tempNum = randomNumber(3,0);
			if (tempNum == 0)
				tempWep = new Scope(tempWep);
			else if (tempNum == 1)
				tempWep = new Stabilizer(tempWep);
			else if (tempNum ==2)
				tempWep = new PowerBooster(tempWep);
			else if(tempNum == 3)
			{}
			Things.get(i).setWeapon(tempWep);
		}
	}
	
	public int randomNumber(int max, int min)
	{
		return (int) (Math.random() * (max - min) + 1);
	}
	//method to add AI to the arrayList
	public void addAI(AIContext ai)
	{
		AI.add(ai);
	}
	
	//method that will loop through all AIContext and perform .takeTurn() on each one.
	public void updateTime(int time)
	{
		currentTime = time;
		
		if (!(AI.isEmpty()));
		{
			for (int i = 0; i < AI.size(); i++)
			{
				(AI.get(i)).takeTurn(currentTime);	 
			}
		}
	}
	
	public int getTime() 
	{
		return currentTime;
	}
	
	public LifeForm getLifeForm(int index)
	{
		return Things.get(index);
	}
	public Weapon getWeapon(int index)
	{
		return MoreThings.get(index);
	}
}
