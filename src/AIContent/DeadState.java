package AIContent;

import environment.Environment;
import lifeform.LifeForm;

public class DeadState implements ActionState
{
	LifeForm lifeform;
	AIContext controller;
	Environment e;
	
	//constructor takes in the life form it will control and the aicontext that has it so that this state can changes the aicontext current state 
	public DeadState(LifeForm lifeform, AIContext aiContext, Environment e) 
	{
		this.lifeform = lifeform;
		controller = aiContext;
		this.e = e;
	}

	@Override
	public void executeAction() {
		int x,y;
		if (lifeform.getWeapon() != null) 
		{
			lifeform.setWeapon(null);
			controller.setNoWeapon();
		}
		
		do {
		 x =	(int) (Math.random() * e.getColumns() - 1);
		 y =	(int) (Math.random() * e.getRows() - 1);
		} while (e.addLifeForm(y, x, lifeform) == false);
		controller.setNoWeapon();
		
		// TODO Auto-generated method stub
		//do something with the lifeform based on logic listed on P.85 of the lab hand out
		//controller.set*insertstate* executeAction may change the current state of the AIContext based on the logic listed on P.85 of the Lab hand out
			
			
		
	}

}
////If the LifeForm had a weapon then remove that weapon and
//place it in a random Cell in the Environment (that has space). Put
//LifeForm back to full life points. Move to No Weapon state.