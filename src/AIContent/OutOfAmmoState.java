package AIContent;

import lifeform.LifeForm;

public class OutOfAmmoState implements ActionState
{
	LifeForm lifeform;
	AIContext controller;
	
	//constructor takes in the life form it will control and the aicontext that has it so that this state can changes the aicontext current state 
	public OutOfAmmoState(LifeForm lifeform, AIContext aiContext) 
	{
		this.lifeform = lifeform;
		controller = aiContext;
	}

	@Override
	public void executeAction() {
		
		if(lifeform.getLifePoints() == 0)
		{
			controller.setDeadState();
		}
		else 
		{
			lifeform.reload();
			controller.setHasWeapon();
		}
	}

}
