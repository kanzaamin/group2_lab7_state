package gameplay;

import exceptions.EnvironmentException;

/*
 * Brad Eddowes
 * 
 * TimeObserver interface for observers
 */
public interface TimeObserver 
{
	//method that all observers of time will use to update stats and functions based on the passeage of time
	public void updateTime(int time);
	//method for checking that the time observer received the time correctly
	public int getTime();
}
