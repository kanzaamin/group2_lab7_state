package gameplay;

import java.util.ArrayList;


/*
 * Brad Eddowes
 * 
 * SimpleTimer of Timer interface to keep track of rounds, observers, and update the observers data
 */
public class SimpleTimer  extends Thread implements Timer
{
	private int round;
	private int timeDelay;
	ArrayList<TimeObserver> theObservers = new ArrayList<TimeObserver>();
	
	/*
	 * constructors Follow
	 */
	public SimpleTimer()
	{
		this(0);
	}
	
	public SimpleTimer(int waitTime)
	{
		round = 0;
		timeDelay = waitTime;
	}
	/**
	 * @param TimeObserver observer to be added to the array
	 */
	public void addTimeObserver(TimeObserver observer)
	{
		observer.updateTime(round);
		theObservers.add(observer);
	}
	
	/**
	 * @param TimeObserver observer to be removed from the array
	 */
	public void removeTimeObserver(TimeObserver observer)
	{
		theObservers.remove(observer);
	}
	
	/*
	 * advance the round
	 */
	public void timeChanged()
	{	
		round += 1;

		if (!(theObservers.isEmpty()));
		{
			for (int i = 0; i < theObservers.size(); i++)
			{
				(theObservers.get(i)).updateTime(round);	
			}
		}
	}
	
	/**
	 * 
	 * @return current timer round
	 */
	public int getRound()
	{
		return round;
	}
	
	/*
	 * threaded operation start method
	 */
	public void run()
	{
		for (int x=0;x<10;x++)
		{
			try
			{
				Thread.sleep(timeDelay);
				timeChanged();
				//System.out.println("Time Changed Called, round updated");
			}
			catch (InterruptedException e)
			{
				System.out.println("Something bad happened.");
			}
		}
	}
	
}
