package gameplay;

/*
 * Brad Eddowes
 * 
 * Timer interface for Timer types
 */
public interface Timer 
{
	//methods that any timer will have
	//method that any timer will user to add observers to its list 
	public void addTimeObserver(TimeObserver observer);
	//method to remove specific observers 
	public void removeTimeObserver(TimeObserver observer);
	//method that all timers will have in order to update all observers of a time change 
	public void timeChanged();
}
