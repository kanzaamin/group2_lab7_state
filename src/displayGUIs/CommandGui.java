package displayGUIs;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;

import javax.net.ssl.SSLException;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import environment.Environment;
import environment.EnvironmentObserver;
import lifeform.Facing;
import lifeform.Human;
import lifeform.LifeForm;
import sun.security.acl.WorldGroupImpl;
import weapon.Pistol;
import weapon.Weapon;

/**
 * @author Andrew McCoy 
 * This class is the commandGui class.
 * This makes the commandGui and acts as a controller
 * for the lifeforms in the game
 */

public class CommandGui extends JFrame implements ActionListener, EnvironmentObserver
{
	/**
	 * Instance variables for the commandGui
	 */
	Command attackCommand, moveCommand, pick_Up_WepOne, pick_Up_WepTwo, reloadCommand, dropCommand, turnNorth, turnSouth, turnWest, turnEast;
	JButton attack, move, pick_up_one, pick_up_two, reload, drop, top, left, right, down;
	Environment world;
	
	/**
	 * Constructor for commandGui holds an instance of the world
	 * @param world
	 */
	public CommandGui(Environment world)
	{
		this.world = world;
		paintGui();
		update();
		
	}
	
	/**
	 * This method paints the specific elements of the gui
	 * including buttons, location of the lifeforms, and more!
	 */
	public void paintGui()
	{
		setLayout(new BorderLayout());
		JPanel west = new JPanel(new GridLayout(3,3));
		JPanel center = new JPanel(new GridLayout(3,3));
		JPanel east = new JPanel(new GridLayout(6,1));
		setAlwaysOnTop (true);
		//Buttons
		attack = new JButton("ATTACK");
		attack.addActionListener(this);
		
		move = new JButton("MOVE");
		move.addActionListener(this);
		
		pick_up_one = new JButton("PICK UP 1");
		pick_up_one.addActionListener(this);
		
		pick_up_two = new JButton("PICK UP 2");
		pick_up_two.addActionListener(this);
		
		reload = new JButton("RELOAD");
		reload.addActionListener(this);
		
		drop = new JButton("DROP");
		drop.addActionListener(this);
		
		top = new JButton("TOP");
		top.addActionListener(this);
		
		left = new JButton("LEFT");
		left.addActionListener(this);
		
		right = new JButton("RIGHT");
		right.addActionListener(this);
		
		down = new JButton("DOWN");
		down.addActionListener(this);
		
		for(int x = 0; x < 9; x++)
		{
			if(x == 1)
			{
				west.add(top);
			}
			else if(x == 3)
			{
				west.add(left);
			}
			else if(x == 5)
			{
				west.add(right);
			}
			else if(x == 7)
			{
				west.add(down);
			}
			else
			{
			west.add(new JLabel(""));
			}
		}
		for(int x = 0; x < 9; x++)
		{
			//Creates a space between the west and east panel
			center.add(new JLabel("              "));
		}
		east.add(attack);
		east.add(move);
		east.add(pick_up_one);
		east.add(pick_up_two);
		east.add(reload);
		east.add(drop);
		add("West",west);
		add("Center",center);
		add("East",east);
		pack();
		setVisible(true);
	}
	
	/**
	 * This method is home to all of the different
	 * actions that the commandGui can do
	 * These are all linked to the lifeform
	 */
	@Override
	public void actionPerformed(ActionEvent e) {
		
		if (e.getSource() == attack)
		{
			System.out.println("Attack used");
			attackCommand.execute();
		}
		else if (e.getSource() == move)
		{
			System.out.println("You are moving");
			moveCommand.execute();
		}
		else if (e.getSource() == pick_up_one)
		{
			System.out.println("You picked up the first weapon");
			pick_Up_WepOne.execute();
			/**
			 * there will need to be a conditional added later to check
			 * if the user already has a weapon and stuff
			 */
		}
		else if (e.getSource() == pick_up_two)
		{
			System.out.println("You picked up the two weapon");
			pick_Up_WepTwo.execute();
			/**
			 * there will need to be a conditional added later to check
			 * if the user already has a weapon and stuff
			 */
		}
		else if (e.getSource() == reload)
		{
			System.out.println("Gun reloaded");
			reloadCommand.execute();
		}
		
		else if (e.getSource() == drop)
		{
			System.out.println("weapon dropped");
			dropCommand.execute();
		}
		
		else if (e.getSource() == top)
		{
			turnNorth.execute();
		}
		
		else if (e.getSource() == down)
		{
			turnSouth.execute();
		}
		
		else if (e.getSource() == left)
		{
			turnWest.execute();
		}
		else if (e.getSource() == right)
		{
			turnEast.execute();
		}
	}
	
	/**
	 * CommandGui update method
	 * This method updates the gui for a number of things like
	 * checking what commands the lifeform can do and what commands 
	 * it should make for the specific cell selected
	 */
	public void update()
	{
		int highX, highY;
		attackCommand = moveCommand = pick_Up_WepOne = pick_Up_WepTwo = reloadCommand = dropCommand = turnNorth = turnSouth = turnWest = turnEast = null;
		highX = world.getHighlightedX();
		highY = world.getHighlightedY();
		LifeForm current = world.getLifeForm(highY, highX);
		
		if (current != null)
		{
			moveCommand = new Move(current);
			turnNorth = new Turn(Facing.North,current);
			turnSouth = new Turn(Facing.South,current);
			turnWest = new Turn(Facing.West,current);
			turnEast = new Turn(Facing.East, current);
			attackCommand = new Attack(current);
			
			if (world.getWeapon(1, highX, highY) != null)
				pick_Up_WepOne = new Acquire(1, current, world); 
			if (world.getWeapon(2, highX, highY) != null)
				pick_Up_WepTwo = new Acquire(2, current, world);
			
			if (current.getWeapon() != null)
			{
				reloadCommand = new Reload(current);
				
				if (world.getWeapon(1, highX, highY) == null || world.getWeapon(2, highX, highY) == null)
				{
					dropCommand = new Drop(current, world);
				}
			}
		}
		//toggle visibles	
		if (turnNorth == null)
			top.setEnabled(false);
		else
			top.setEnabled(true);
		if (turnSouth == null)
			down.setEnabled(false);
		else
			down.setEnabled(true);
		if (turnWest == null)
			left.setEnabled(false);
		else
			left.setEnabled(true);
		if (turnEast == null)
			right.setEnabled(false);
		else
			right.setEnabled(true);
		if (dropCommand == null)
			drop.setEnabled(false);
		else
			drop.setEnabled(true);
		if (attackCommand == null)
			attack.setEnabled(false);
		else
			attack.setEnabled(true);
		if (reloadCommand == null)
			reload.setEnabled(false);
		else
			reload.setEnabled(true);
		if (moveCommand == null)
			move.setEnabled(false);
		else
			move.setEnabled(true);
		if (pick_Up_WepOne == null)
			pick_up_one.setEnabled(false);
		else
			pick_up_one.setEnabled(true);
		if (pick_Up_WepTwo == null)
			pick_up_two.setEnabled(false);
		else
			pick_up_two.setEnabled(true);
	}


}

