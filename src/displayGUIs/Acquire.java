/*
 * group 2
 * @Bradley Eddowes & Isaiah English & Kanza Amin Test
 */

package displayGUIs;

import environment.Environment;
import lifeform.LifeForm;

public class Acquire implements Command 
{
	//holds the slot the command will pick up from, the lifeform to use and environment for data
	private int slot;
	private LifeForm thing;
	private Environment world;
	//takes in slot number, lifeform and environment for use
	public Acquire(int slot, LifeForm thing, Environment world)
	{
		this.slot = slot;
		this.thing = thing;
		this.world = world;
	}
	
	@Override // method that invoker will call to try and have lifeform pick up a weapon
	public void execute() 
	{
		if (thing.getWeapon() != null)
			thing.setWeapon(world.exchangeWeapon(thing.removeWeapon(), slot, world.getHighlightedX(), world.getHighlightedY()));
		else if (thing.getWeapon() == null)
			thing.setWeapon(world.removeWeaponFromCell(slot, world.getHighlightedX(), world.getHighlightedY()));
	}

}
