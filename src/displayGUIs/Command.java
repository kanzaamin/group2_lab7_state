/*
 * group 2
 * @Bradley Eddowes & Isaiah English & Kanza Amin Test
 */

package displayGUIs;

import lifeform.LifeForm;

public interface Command 
{
	//method in all commands to execute action for a lifeform
	public void execute();
}
