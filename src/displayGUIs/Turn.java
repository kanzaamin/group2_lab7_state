/*
 * group 2
 * @Bradley Eddowes & Isaiah English & Kanza Amin Test
 */

package displayGUIs;

import lifeform.Facing;
import lifeform.LifeForm;


public class Turn implements Command 
{
	//hold the lifeform to turn and the direction the command will turn the lifeform to
	private Facing direction;
	private LifeForm thing;
	
	//constructor will set which direction it will turn the life form as well as receive the lifeform to turn
	public Turn(Facing direction, LifeForm thing)
	{
		this.direction = direction;
		this.thing = thing;
	}
	
	//the execute command method being overridden to change direction of lifeform
	@Override
	public void execute() 
	{
		thing.changeDirection(direction);
	}

}
