package displayGUIs;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import environment.Environment;
import environment.EnvironmentObserver;
import lifeform.Alien;
import lifeform.Human;
import lifeform.LifeForm;
import recovery.RecoveryBehavior;
import recovery.RecoveryFractional;
import recovery.RecoveryLinear;
import recovery.RecoveryNone;
import weapon.Pistol;
import weapon.Scope;
import weapon.Weapon;
/**
 * i have just been working on how the gui will look like. having trouble figuring out how to
 * position north and south. n, s, and w all shift to the left. any suggestions?
 * @author ka7989
 *
 *
 *change the center panel to a grid layout, loop through adding a button
 *method for the drawings/icons
 * if lifeform? this. if weapon? this ...
 */

//this method prints out the grid
public class EnvironmentGui extends JFrame  implements ActionListener, EnvironmentObserver
{
	int buttonClicked = 0;
	JButton textButton, imageButton, gridButton;
	JLabel NorthTextLabel, SouthTextLabel, EastTextLabel, WestTextLabel, imageLabel, displayLabel;
	Color buttonColor = new Color(250,0,0, 250);
	Color buttonColor1 = new Color(0,0,250,250);
	Color buttonColor2 = new Color(0, 250, 0, 250);
	Color buttonColor3 = new Color(250, 0, 0, 250);
	JButton buttons[][] = new JButton[10][10];

	String humAli, lifeName, wepNum, wepName, RECtype;
	int LPnum, APnum;


	Environment env;
	public EnvironmentGui(Environment env)
	{
// this method defines the images
		ImageIcon icon = new ImageIcon(getClass().getResource("Icons/sun.gif"));
		ImageIcon human = new ImageIcon(getClass().getResource("Icons/lifeform.png"));
		ImageIcon human1weapon = new ImageIcon(getClass().getResource("Icons/human1weapon.png"));
		ImageIcon human2weapon = new ImageIcon(getClass().getResource("Icons/human2weapon.png"));
		ImageIcon alien = new ImageIcon(getClass().getResource("Icons/alien.png"));
		ImageIcon alien1weapon = new ImageIcon(getClass().getResource("Icons/alien1weapon.png"));
		ImageIcon alien2weapon = new ImageIcon(getClass().getResource("Icons/alien2weapons.png"));
		ImageIcon legend = new ImageIcon(getClass().getResource("Icons/legend.png"));
//below is the legend and the point tracker

		this.env = env;
		textButton = new JButton(legend);
		textButton.addActionListener(this);
		add("East",textButton);
		displayLabel = new JLabel("Cell is at "+env.getHighlightedX() +" "+ env.getHighlightedY()+"| Cell has ___weapons   | Human LifePoints:___| Armor Points:___| Weapon:_____");
		add("South", displayLabel);




		JPanel centerPanel = new JPanel(new GridLayout(env.getRows(), env.getColumns()));

//		for (int r=0;r<(env.getRows());r++)
//		{
//			for (int c=0;c<(env.getColumns());c++)
//			{
//
//				//HUMAN PICTURES
//				if (env.getLifeForm(r, c)instanceof Human )
//				{
//
//					if (env.getWeapon(1, r,c) != null)
//					{
//						//here goes the picture for human and 1 weapon
//						buttons[r][c] = new JButton(human1weapon);					
//						buttons[r][c].addActionListener(this);
//					}
//					else if (env.getWeapon(2, r, c) !=null) 
//					{
//						//here goes the picture for human and 2 weapons
//						buttons[r][c]=new JButton(human2weapon);
//						buttons[r][c].addActionListener(this);
//					}
//					else
//					{
//						//here does the picture for human and 0 weapons
//						buttons[r][c]=new JButton(human);
//						buttons[r][c].addActionListener(this);
//					}
//
//				}
//				//ALIEN PICTURES
//				if (env.getLifeForm(r, c)instanceof Alien )
//				{
//
//					if (env.getWeapon(1, r,c) != null)
//					{
//						//here goes the picture for human and 1 weapon
//						buttons[r][c] = new JButton(alien1weapon);
//						buttons[r][c].addActionListener(this);
//
//					}
//					else if (env.getWeapon(2, r, c) !=null) 
//					{
//						//here goes the picture for human and 2 weapons
//						buttons[r][c]=new JButton(alien2weapon);
//						buttons[r][c].addActionListener(this);
//					}
//					else
//					{
//						//here does the picture for human and 0 weapons
//						buttons[r][c]=new JButton(alien);
//						buttons[r][c].addActionListener(this);
//					}				
//					centerPanel.add(buttons[r][c]);
//
//			}
//		}

//this prints out the grid
		for (int r=0;r<(env.getRows());r++)
		{
			for (int c=0;c<(env.getColumns());c++)
			{
				//add a button to each cell!
				buttons[r][c] = new JButton(icon);
				buttons[r][c].addActionListener(this);
				centerPanel.add(buttons[r][c]);
			}

		}
		add("Center",centerPanel);
		pack();
		setVisible(true);
	}
	
	
	public static void main (String [] args)
	{
//main method that renders the environment
		Environment bob = Environment.getInstance(10, 10);
		EnvironmentGui fred = new EnvironmentGui(bob);
		CommandGui steve = new CommandGui(bob);
		bob.addEnvironmentObserver(fred);
		bob.addEnvironmentObserver(steve);
		LifeForm andrew = new Human("andrew", 10000, 1);
		Weapon gun = new Pistol("The Destroyerrr");
		gun = new Scope(gun);
		andrew.setWeapon(gun);
		bob.addLifeForm(0,0, andrew);

		Alien reed = new Alien("reed", 99, new RecoveryLinear(1));
		bob.addLifeForm(0, 3, reed);

	}
	@Override
	public void actionPerformed(ActionEvent event)
	{
//this sets the background depending on alien or human
		for (int r=0;r<(env.getRows());r++)
		{
			for (int c=0;c<(env.getColumns());c++)
			{
				buttons[r][c].setBackground(null);

				System.out.println(r + " " + c);
			}
		}
		System.out.println("HI");
		if (event.getSource() == textButton)
		{
			if(buttonClicked == 0)
			{
				buttonClicked = 1;
				textButton.setBackground(buttonColor);
			}
			else
			{
				buttonClicked = 0;
				textButton.setBackground(buttonColor1);
			}

		}
		if (event.getSource() == gridButton)
		{
			gridButton.setText(":D");

		}

		for (int r=0;r<(env.getRows());r++)
		{
			for (int c=0;c<(env.getColumns());c++)
			{

				if(event.getSource() == buttons[r][c])
				{
					buttons[r][c].setBackground(buttonColor1);
					//LOOK AT THIS L8R ON 
					env.setHighlightedCell(c, r);
					System.out.println(r + " "+ c);
				}
			}
		}
	}
	public void paintGridGui()
	{

		for (int r=0;r<(env.getRows());r++)
		{
			for (int c=0;c<(env.getColumns());c++)
			{
				if(env.getLifeForm(r, c) != null)
				{
					if(env.getLifeForm(r, c) instanceof Human)
					{
						buttons[r][c].setBackground(buttonColor2);
//						if(env.getLifeForm(r,c).getWeapon() != null)
//						{
//							buttons[r][c].setImage(icon2);
//							
//						}
//						else
//						{
//							wepName= "N/A";
//						}
					}
					else if(env.getLifeForm(r, c) instanceof Alien)
					{
						buttons[r][c].setBackground(buttonColor3);

					}
				}
				else if (buttons[r][c].getBackground() != buttonColor1)
					buttons[r][c].setBackground(null);
			}
		}

		int tmp = 0;
		if (env.getWeapon(1, env.getHighlightedX(), env.getHighlightedY()) != null)
		{
			tmp++;
		}
		if (env.getWeapon(2, env.getHighlightedX(), env.getHighlightedY()) != null)
		{
			tmp++;
		}
//checks if alien or human 
		if (env.getLifeForm(env.getHighlightedY(), env.getHighlightedX()) != null)
		{
			if (env.getLifeForm(env.getHighlightedY(), env.getHighlightedX()) instanceof Human)
			{
				humAli = "Human";
				Human temp = (Human) env.getLifeForm(env.getHighlightedY(), env.getHighlightedX());
				APnum = temp.getArmorPoints();
			}
			else if (env.getLifeForm(env.getHighlightedY(), env.getHighlightedX()) instanceof Alien)
			{
				humAli = "Alien";
				Alien temp = (Alien)env.getLifeForm(env.getHighlightedY(), env.getHighlightedX());
// this checks recovery behavior
				RecoveryBehavior rb = temp.getRecoveryRate();
				if(rb instanceof RecoveryFractional)
				{
					RECtype = "Fractional Recovery";
				}
				else if(rb instanceof RecoveryLinear)
				{
					RECtype = "Linear Recovery";
				}
				if(rb instanceof RecoveryNone)
				{
					RECtype = "No Recovery";
				}
			}
			else
			{
				humAli = null;
			}

			if(env.getLifeForm(env.getHighlightedY(), env.getHighlightedX()).getWeapon() != null)
			{
				wepName = env.getLifeForm(env.getHighlightedY(), env.getHighlightedX()).getWeapon().getName();
				
			}
			else
			{
				wepName= "N/A";
			}


			LPnum = env.getLifeForm(env.getHighlightedY(), env.getHighlightedX()).getLifePoints();
			lifeName = env.getLifeForm(env.getHighlightedY(), env.getHighlightedX()).getName();



			if (tmp == 0)
				wepNum = "Zero";
			else if (tmp == 1)
				wepNum = "One";
			else if (tmp == 2)
				wepNum = "Two";

//this is the label of what cell, how many lifepoints, etc
			if (humAli == "Human")
				displayLabel.setText("Cell selected is "+env.getHighlightedX() +"," + env.getHighlightedY()+" | Cell has "+wepNum+" weapons   | "+humAli+" Named: " +lifeName+" LifePoints: "+LPnum+" | Armor Points:" + APnum+" | Weapon: "+ wepName);
			else if (humAli == "Alien")
				displayLabel.setText("Cell selected is "+env.getHighlightedX() +"," + env.getHighlightedY()+" | Cell has "+wepNum+" weapons   | "+humAli+" Named: " +lifeName+" LifePoints: "+LPnum+" | Recovery Type:" + RECtype+" | Weapon: "+ wepName);
			else
				displayLabel.setText("Cell selected is "+env.getHighlightedX() +"," + env.getHighlightedY()+" | Cell has "+wepNum+" weapons");

		}
	}
	@Override
	public void update()
	{

		paintGridGui();
	}

}



//add an update method, when something changes in an gui
//should check for highlighted cell, that is what we will change
//it updates the highlighted cell
//if cell has lifeform, print lifeform
//if cell has weapon, print weapon
//where do we find the cell

