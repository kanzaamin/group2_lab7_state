/*
 * group 2
 * @Bradley Eddowes & Isaiah English & Kanza Amin Test
 */

package displayGUIs;

import environment.Environment;
import lifeform.LifeForm;

public class Drop implements Command 
{
	//stores lifeform for use and world for info
	private LifeForm thing;
	private Environment world;
	//takes in lifeform and the environment for use
	public Drop(LifeForm thing, Environment world)
	{
		this.thing = thing;
		this.world = world;
	}
	
	@Override // method to try and have lifeform drop a weapon
	public void execute() 
	{
		// if the first slot is empty, drop there
		if (world.getWeapon(1, thing.getX(), thing.getY()) == null)
			world.addWeaponToCell(thing.removeWeapon(), 1, thing.getX(), thing.getY());
		//if the second slot is empty, drop there
		else if (world.getWeapon(2, thing.getX(), thing.getY()) == null)
			world.addWeaponToCell(thing.removeWeapon(), 2, thing.getX(), thing.getY());
		//otherwise do nothing
	}

}
