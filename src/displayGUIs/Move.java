/*
 * group 2
 * @Bradley Eddowes & Isaiah English & Kanza Amin Test
 */

package displayGUIs;

import environment.Environment;
import lifeform.LifeForm;

public class Move implements Command 
{
	//lifeform the command has to use
	private LifeForm thing;
	//takes in lifeform for use 
	public Move(LifeForm thing)
	{
		this.thing = thing;
	}
	
	@Override//method that the invoker will call to have environment relocate a lifeform
	public void execute() 
	{
		Environment.move(thing);
	}

}
