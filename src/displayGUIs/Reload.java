/*
 * group 2
 * @Bradley Eddowes & Isaiah English & Kanza Amin Test
 */

package displayGUIs;

import lifeform.LifeForm;

public class Reload implements Command 
{
	//life form to use
	private LifeForm thing;
	//takes in lifeform to use
	public Reload(LifeForm thing)
	{
		this.thing = thing;
	}
	@Override // method the envoker will call to reload the lifeforms weapon
	public void execute() 
	{
		thing.reload();
	}

}
