/*
 * group 2
 * @Bradley Eddowes & Isaiah English & Kanza Amin Test
 */

package displayGUIs;

import environment.Environment;
import exceptions.EnvironmentException;
import lifeform.LifeForm;

public class Attack implements Command 
{
	//lifeform the command will use
	private LifeForm thing;
	//takes in the lifeform for use
	public Attack(LifeForm thing)
	{
		this.thing = thing;
	}
	@Override//method that the invoker will call to attack
	public void execute() 
	{
		try 
		{
			//have environment determine the lifeform, if any, that the attack will effect giving it the attacking lifeform
			Environment.attack(thing);
		} catch (EnvironmentException e) 
		{
			System.out.println("Something Broke");
		}
	}

}
